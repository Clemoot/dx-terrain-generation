#pragma once

#include "stdafx.h"
#include "DXDevice.h"

struct ConstantBufferStruct {
	DirectX::XMFLOAT4X4 world;
	DirectX::XMFLOAT4X4 view;
	DirectX::XMFLOAT4X4 projection;
	DirectX::XMFLOAT4X4 mvp;
};

class DXPipeline {
public:
	DXPipeline(const DXDevice& device, const std::string& vertexShader, const std::string& pixelShader, const std::string& geometryShader = "");
	~DXPipeline();
	
	void load(const DXDevice& device) const;

	ConstantBufferStruct& constantBuffer();

private:
	bool readShaderBytecode(const std::string path, unsigned char*& bytecode, unsigned int& bytecodeSize);

	ConstantBufferStruct m_ConstantBufferData;
	Microsoft::WRL::ComPtr<ID3D11VertexShader> m_VertexShader;
	Microsoft::WRL::ComPtr<ID3D11GeometryShader> m_GeometryShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader> m_PixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout> m_InputLayout;
	Microsoft::WRL::ComPtr<ID3D11Buffer> m_ConstantBuffer;
};