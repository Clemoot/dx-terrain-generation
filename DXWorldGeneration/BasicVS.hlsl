cbuffer Matrices : register(b0) {
    float4x4 world;
    float4x4 view;
    float4x4 projection;
    float4x4 mvp;
};

cbuffer Material : register(b1) {
    float4 ambient;
    float4 diffuse;
    float4 specular;
};

struct VS_INPUT {
    float4 pos : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
    float4 color : COLOR;
};

struct VS_OUTPUT {
    float4 pos : SV_POSITION;
    float4 worldPos : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
    float4 color : COLOR;
};

VS_OUTPUT main(VS_INPUT i)
{
    VS_OUTPUT o;
    float4 objPos = i.pos;
    o.pos = mul(objPos, mvp);
    o.worldPos = mul(objPos, world);
    o.normal = i.normal;
    o.uv = i.uv;
    o.color = i.color;

    return o;
}