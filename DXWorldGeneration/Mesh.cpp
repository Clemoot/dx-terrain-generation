#include "stdafx.h"
#include "Mesh.h"

Mesh Mesh::createPlane(const DXDevice& device, const uint16_t& length)
{
	Mesh plane;
	if (length == 0) {
		LOG("cannot create plane with length 0");
		return plane;
	}

	std::vector<DirectX::XMFLOAT3> vertices;
	std::vector<DirectX::XMFLOAT3> normals;
	std::vector<DirectX::XMFLOAT2> uvs;
	std::vector<DirectX::XMFLOAT3> colors;
	std::vector<uint16_t> indices;

	uint16_t rowPitch = 1 + length;
	uint16_t depthPitch = rowPitch * rowPitch;

	vertices.reserve(depthPitch);
	colors.reserve(depthPitch);
	uvs.reserve(depthPitch);
	indices.reserve(length * length * 6);

	float vertexSpace = 1.f / static_cast<float>(length);
	for (float x = -0.5f, uv_x = 0.0f; x <= 0.5f; x += vertexSpace, uv_x += vertexSpace)
		for (float y = -0.5f, uv_y = 0.0f; y <= 0.5f; y += vertexSpace, uv_y += vertexSpace) {
			vertices.push_back(DirectX::XMFLOAT3(x, 0.f, y));
			uvs.push_back(DirectX::XMFLOAT2(uv_x, uv_y));
			colors.push_back(DirectX::XMFLOAT3(uv_x, uv_x, uv_y));
			normals.push_back(DirectX::XMFLOAT3(0.f, 1.f, 0.f));
		}

	for (uint16_t x = 0; x < length; x++) {
		for (uint16_t y = 0; y < length; y++) {
			uint16_t TL = x * rowPitch + y;		// top left
			uint16_t TR = (x+1) * rowPitch + y;	// top right
			uint16_t BL = x * rowPitch + y + 1;	// bottom left
			uint16_t BR = (x+1) * rowPitch + y + 1;	// bottom right

			indices.push_back(TL);
			indices.push_back(TR);
			indices.push_back(BL);

			indices.push_back(TR);
			indices.push_back(BR);
			indices.push_back(BL);
		}
	}

	plane.setVertices(vertices);
	plane.setColors(colors);
	plane.setUVs(uvs);
	plane.setIndices(indices);

	//plane.setVertices({ 
	//	DirectX::XMFLOAT3(-0.5f, 0.0f, -0.5f),
	//	DirectX::XMFLOAT3(-0.5f, 0.0f,  0.5f),
	//	DirectX::XMFLOAT3( 0.5f, 0.0f, -0.5f),
	//	DirectX::XMFLOAT3( 0.5f, 0.0f,  0.5f),
	//});
	//plane.setColors({
	//	DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
	//	DirectX::XMFLOAT3(0.3f, 0.3f, 0.3f),
	//	DirectX::XMFLOAT3(0.6f, 0.6f, 0.6f),
	//	DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f)
	//});
	//plane.setUVs({
	//	DirectX::XMFLOAT2(0.f, 0.f),
	//	DirectX::XMFLOAT2(0.f, 1.f),
	//	DirectX::XMFLOAT2(1.f, 0.f),
	//	DirectX::XMFLOAT2(1.f, 1.f),
	//});
	//plane.setIndices({
	//	0, 2, 1,
	//	2, 3, 1
	//});
	plane.apply(device);

	return plane;
}

Mesh Mesh::createCube(const DXDevice& device)
{
	Mesh cube;
	cube.setVertices({
		DirectX::XMFLOAT3(-0.5f	,	-0.5f	,	-0.5f),
		DirectX::XMFLOAT3(-0.5f	,	-0.5f	,	 0.5f),
		DirectX::XMFLOAT3(-0.5f	,	 0.5f	,	-0.5f),
		DirectX::XMFLOAT3(-0.5f	,	 0.5f	,	 0.5f),
		DirectX::XMFLOAT3(0.5f	,	-0.5f	,	-0.5f),
		DirectX::XMFLOAT3(0.5f	,	-0.5f	,	 0.5f),
		DirectX::XMFLOAT3(0.5f	,	 0.5f	,	-0.5f),
		DirectX::XMFLOAT3(0.5f	,	 0.5f	,	 0.5f)
	});
	cube.setColors({
		DirectX::XMFLOAT3(0, 0,	0),
		DirectX::XMFLOAT3(0, 0,	1),
		DirectX::XMFLOAT3(0, 1,	0),
		DirectX::XMFLOAT3(0, 1,	1),
		DirectX::XMFLOAT3(1, 0,	0),
		DirectX::XMFLOAT3(1, 0,	1),
		DirectX::XMFLOAT3(1, 1,	0),
		DirectX::XMFLOAT3(1, 1,	1)
	});
	cube.setIndices({
		0, 2, 1,
		1, 2, 3,
		4, 5, 6,
		5, 7, 6,
		0, 1, 5,
		0, 5, 4,
		2, 6, 7,
		2, 7, 3,
		0, 4, 6,
		0, 6, 2,
		1, 3, 7,
		1, 7, 5
	});
	cube.apply(device);

	return cube;
}

Mesh::Mesh():
	m_VertexBufferState{ 0, false, false },
	m_IndexBufferState{ 0, false, false },
	m_VertexBuffer(nullptr),
	m_IndexBuffer(nullptr),
	m_Vertices{},
	m_Colors{},
	m_Indices{}
{
}

Mesh::~Mesh()
{
	m_VertexBuffer.Reset();
	m_IndexBuffer.Reset();
}

void Mesh::setVertices(const std::vector<DirectX::XMFLOAT3>& vertices)
{
	m_Vertices = vertices;
	m_VertexBufferState.updateData = true;

	if (m_VertexBufferState.size < vertices.size())
		m_VertexBufferState.reallocateBuffers = true;
	else
		m_VertexBufferState.reallocateBuffers = false;
}

void Mesh::setNormals(const std::vector<DirectX::XMFLOAT3>& normals)
{
	m_Normals = normals;
	m_VertexBufferState.updateData = true;
}

void Mesh::setUVs(const std::vector<DirectX::XMFLOAT2>& uvs)
{
	m_UVs = uvs;
	m_VertexBufferState.updateData = true;
}

void Mesh::setColors(const std::vector<DirectX::XMFLOAT3>& colors)
{
	m_Colors = colors;
	m_VertexBufferState.updateData = true;
}

void Mesh::setIndices(const std::vector<uint16_t>& indices)
{
	m_Indices = indices;
	m_IndexBufferState.updateData = true;

	if (m_IndexBufferState.size < indices.size())
		m_IndexBufferState.reallocateBuffers = true;
	else
		m_IndexBufferState.reallocateBuffers = false;
}

void Mesh::setMaterial(const std::shared_ptr<Material>& material)
{
	m_Material = material;
}

void Mesh::calculateNormals()
{
	m_Normals.clear();
	m_Normals.reserve(m_Vertices.size());

	std::vector<DirectX::XMVECTOR> tmpNormals;
	tmpNormals.resize(m_Vertices.size(), { 0.f, 0.f, 0.f });

	for (size_t i = 0, nbIndices = m_Indices.size(); i < nbIndices; i += 3) {
		DirectX::XMVECTOR vertices[] = {
			DirectX::XMLoadFloat3(&m_Vertices[m_Indices[i]]),
			DirectX::XMLoadFloat3(&m_Vertices[m_Indices[i + 1]]),
			DirectX::XMLoadFloat3(&m_Vertices[m_Indices[i + 2]])
		};

		DirectX::XMVECTOR faceNormal =
			DirectX::XMVector3Normalize(
				DirectX::XMVector3Cross(
					DirectX::XMVectorSubtract(vertices[2], vertices[0]),
					DirectX::XMVectorSubtract(vertices[1], vertices[0])
				)
			);

		tmpNormals[m_Indices[i + 0]] = DirectX::XMVectorAdd(tmpNormals[m_Indices[i + 0]], faceNormal);
		tmpNormals[m_Indices[i + 1]] = DirectX::XMVectorAdd(tmpNormals[m_Indices[i + 1]], faceNormal);
		tmpNormals[m_Indices[i + 2]] = DirectX::XMVectorAdd(tmpNormals[m_Indices[i + 2]], faceNormal);
	}

	for (size_t i = 0, nbNormals = tmpNormals.size(); i < nbNormals; i++) {
		DirectX::XMFLOAT3 normal;
		DirectX::XMStoreFloat3(
			&normal,
			DirectX::XMVector3Normalize(tmpNormals[i])
		);
		m_Normals.push_back(normal);
	}
}

void Mesh::apply(const DXDevice& device)
{
	if (m_Vertices.size() != m_Colors.size()) {
		LOG("number of vertices and number of colors do not match");
		return;
	}

	updateVertexBuffer(device);
	updateIndexBuffer(device);
}

void Mesh::attach(const DXDevice& device) const
{
	uint32_t stride = sizeof(VertexData), offset = 0;
	device.context()->IASetVertexBuffers(0, 1, m_VertexBuffer.GetAddressOf(), &stride, &offset);
	device.context()->IASetIndexBuffer(m_IndexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0);
	device.context()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if (m_Material)
		m_Material->attach(device);
}

void Mesh::draw(const DXDevice& device) const
{
	device.context()->DrawIndexed(m_IndexBufferState.size, 0, 0);
}

const std::shared_ptr<Material>& Mesh::material() const
{
	return m_Material;
}

void Mesh::updateVertexBuffer(const DXDevice& device)
{
	if (!m_VertexBufferState.updateData && !m_VertexBufferState.reallocateBuffers)
		return;

	size_t size = m_Vertices.size();
	VertexData* vertexData = new VertexData[size];

	for (size_t i = 0; i < size; i++) {
		vertexData[i].position = DirectX::XMFLOAT4(m_Vertices[i].x, m_Vertices[i].y, m_Vertices[i].z, 1.f);
		vertexData[i].uv = m_UVs[i];
		vertexData[i].color = DirectX::XMFLOAT4(m_Colors[i].x, m_Colors[i].y, m_Colors[i].z, 1.f);
	}

	if (!m_Normals.empty())
		for (size_t i = 0; i < size; i++)
			vertexData[i].normal = m_Normals[i];
	else
		for (size_t i = 0; i < size; i++)
			vertexData[i].normal = DirectX::XMFLOAT3(0.f, 0.f, 0.f);

	if (m_VertexBufferState.reallocateBuffers) {
		if (m_VertexBuffer.Get() != nullptr) {
			m_VertexBuffer->Release();
			m_VertexBuffer.Reset();
		}

		CD3D11_BUFFER_DESC desc(
			size * sizeof(VertexData),
			D3D11_BIND_VERTEX_BUFFER,
			D3D11_USAGE_DYNAMIC,
			D3D11_CPU_ACCESS_WRITE
		);

		D3D11_SUBRESOURCE_DATA data;
		data.pSysMem = vertexData;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		if (FAILED(device.device()->CreateBuffer(&desc, &data, m_VertexBuffer.GetAddressOf()))) {
			LOG("failed to create/reallocate vertex buffer");
		}

		m_VertexBufferState.size = size;
		m_VertexBufferState.updateData = false;
		m_VertexBufferState.reallocateBuffers = false;
	}
	else if (m_VertexBufferState.updateData) {
		if (m_VertexBuffer.Get() == nullptr) {
			LOG("tried to update data of non-initialized vertex buffer");
			return;
		}

		D3D11_MAPPED_SUBRESOURCE resource{};
		ZeroMemory(&resource, sizeof(resource));

		device.context()->Map(m_VertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		memcpy(resource.pData, vertexData, size * sizeof(VertexData));
		device.context()->Unmap(m_VertexBuffer.Get(), 0);

		m_VertexBufferState.updateData = false;
	}

	delete[] vertexData;
}

void Mesh::updateIndexBuffer(const DXDevice& device)
{
	if (!m_IndexBufferState.updateData && !m_IndexBufferState.reallocateBuffers)
		return;

	if (m_IndexBufferState.reallocateBuffers) {
		if (m_IndexBuffer.Get() != nullptr) {
			m_IndexBuffer->Release();
			m_IndexBuffer.Reset();
		}

		CD3D11_BUFFER_DESC desc(
			m_Indices.size() * sizeof(uint16_t),
			D3D11_BIND_INDEX_BUFFER,
			D3D11_USAGE_DYNAMIC,
			D3D11_CPU_ACCESS_WRITE
		);

		D3D11_SUBRESOURCE_DATA data;
		data.pSysMem = m_Indices.data();
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		if (FAILED(device.device()->CreateBuffer(&desc, &data, m_IndexBuffer.GetAddressOf()))) {
			LOG("failed to create/reallocate vertex buffer");
		}

		m_IndexBufferState.size = m_Indices.size();
		m_IndexBufferState.updateData = false;
		m_IndexBufferState.reallocateBuffers = false;
	}
	else if (m_IndexBufferState.updateData) {
		if (m_IndexBuffer.Get() == nullptr) {
			LOG("tried to update data of non-initialized index buffer");
			return;
		}

		D3D11_MAPPED_SUBRESOURCE resource{};
		ZeroMemory(&resource, sizeof(resource));

		device.context()->Map(m_IndexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		memcpy(resource.pData, m_Indices.data(), m_Indices.size() * sizeof(uint16_t));
		device.context()->Unmap(m_IndexBuffer.Get(), 0);

		m_IndexBufferState.updateData = false;
	}
}
