#include "stdafx.h"
#include "SceneObject.h"

SceneObject::SceneObject(std::string name):
	SceneNode(name)
{
}

SceneObject::~SceneObject()
{
}

void SceneObject::addMesh(const Mesh& mesh)
{
	m_Meshes.push_back(mesh);
}

void SceneObject::render(const DXDevice& device, const DirectX::XMMATRIX& transform) const
{
	for (const Mesh& mesh : m_Meshes) {
		DirectX::XMStoreFloat4x4(
			&mesh.material()->pipeline()->constantBuffer().world,
			DirectX::XMMatrixTranspose(
				transform
			)
		);

		DirectX::XMStoreFloat4x4(
			&mesh.material()->pipeline()->constantBuffer().mvp,
			DirectX::XMMatrixMultiply(
				DirectX::XMLoadFloat4x4(&mesh.material()->pipeline()->constantBuffer().projection),
				DirectX::XMMatrixMultiply(
					DirectX::XMLoadFloat4x4(&mesh.material()->pipeline()->constantBuffer().view),
					DirectX::XMLoadFloat4x4(&mesh.material()->pipeline()->constantBuffer().world)
				)
			)
		);
	}

	size_t nbMeshes = m_Meshes.size();
	for (size_t i = 0; i < nbMeshes; i++) {
		m_Meshes[i].attach(device);
		m_Meshes[i].draw(device);
	}
}

const std::vector<Mesh>& SceneObject::meshes() const
{
	return m_Meshes;
}
