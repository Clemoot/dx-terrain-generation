#include "stdafx.h"
#include "App.h"

#include <iomanip>

App* App::Instance = nullptr;

App::App() :
	m_Window("DXWorldGeneration", 1600, 1200),
	m_Device(), m_Swapchain(m_Window, m_Device),
	m_SceneRoot(new SceneNode("Scene root")),
	m_Objects{},
	m_Camera(new ObservationCamera("Main camera")),
	m_SelectedObjectIndex(1)
{
	Instance = this;

	m_Window.onResize() = OnWindowResize;

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplWin32_Init(m_Window.handle());
	ImGui_ImplDX11_Init(m_Device.device().Get(), m_Device.context().Get());

	m_Window.show();

	ZeroMemory(&m_Viewport, sizeof(m_Viewport));
	m_Viewport.Width = static_cast<float>(m_Swapchain.renderTarget().texDesc().Width);
	m_Viewport.Height = static_cast<float>(m_Swapchain.renderTarget().texDesc().Height);
	m_Viewport.MinDepth = 0;
	m_Viewport.MaxDepth = 1;

	m_Objects[0] = new PerlinTerrainObject("Perlin", m_Device);
	m_Objects[1] = new TerrainContainer<VoronoiTerrain>("Voronoi Terrain", m_Device, 500, 2);
	//m_Objects[2] = new TerrainContainer<MarchingCubesTerrain>("Marching Cubes Terrain", m_Device);
	
	//GenerativeTerrainParam param("A", 10, 20);

	m_SceneRoot->addChild(m_Objects[0]);
	m_SceneRoot->addChild(m_Objects[1]);
	m_SceneRoot->addChild(m_Objects[2]);
	m_SceneRoot->addChild(m_Camera);

	switch (m_SelectedObjectIndex) {
	case 0:
		m_Objects[1]->disable();
		//m_Objects[2]->disable();
		break;
	case 1:
		m_Objects[0]->disable();
		//m_Objects[2]->disable();
		break;
	case 2:
		m_Objects[1]->disable();
		//m_Objects[2]->disable();
		break;
	}

	m_Device.dumpInfoQueue();
}

App::~App()
{
	SceneNode::deleteNodeAndChildren(m_SceneRoot);

	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

void App::run()
{
	MSG msg{0};

	bool opened = true;
	SceneObject* selectedObject = nullptr;

	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, m_Window.handle(), 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {

			ImGui_ImplDX11_NewFrame();
			ImGui_ImplWin32_NewFrame();
			ImGui::NewFrame();

			//ImGui::ShowDemoWindow(&opened);
			ImGui::Begin("Options");
			ImGui::SetWindowSize(ImVec2(250, 250), ImGuiCond_FirstUseEver);

			ImGui::RadioButton("Basic perlin noise", &m_SelectedObjectIndex, 0);
			ImGui::RadioButton("Voronoi-based generation", &m_SelectedObjectIndex, 1);

			switch (m_SelectedObjectIndex) {
			case 0:
				m_Objects[0]->enable();
				m_Objects[1]->disable();
				//m_Objects[2]->disable();
				break;
			case 1:
				m_Objects[0]->disable();
				m_Objects[1]->enable();
				//m_Objects[2]->disable();
				break;
			case 2:
				m_Objects[0]->disable();
				m_Objects[1]->disable();
				//m_Objects[2]->enable();
				break;
			}
			selectedObject = m_Objects[m_SelectedObjectIndex];

			m_SceneRoot->draw_imgui();

			ImGui::End();
			ImGui::Render();

			m_SceneRoot->update(m_Device);

			DirectX::XMStoreFloat4x4(
				&selectedObject->meshes()[0].material()->pipeline()->constantBuffer().view,
				DirectX::XMMatrixTranspose(
					m_Camera->getViewMatrix()
				)
			);

			DirectX::XMStoreFloat4x4(
				&selectedObject->meshes()[0].material()->pipeline()->constantBuffer().projection,
				DirectX::XMMatrixTranspose(
					m_Camera->getProjectionMatrix()
				)
			);
			
			const float teal[] = { 0.098f, 0.439f, 0.439f, 1.000f };
			m_Swapchain.clear(m_Device, teal, 1.0f, 0);
			m_Swapchain.setTarget(m_Device);

			m_Device.context()->RSSetViewports(1, &m_Viewport);

			m_SceneRoot->draw(m_Device);

			ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

			m_Swapchain.present();
		}

		m_Device.dumpInfoQueue();
	}
}

void App::OnWindowResize(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	App& app = *Instance;
	
	uint32_t width = LOWORD(lParam);
	uint32_t height = HIWORD(lParam);

	app.device().context()->OMSetRenderTargets(0, 0, 0);
	app.m_Swapchain.resize(app.m_Device);
	app.m_Viewport.Width  = static_cast<float>(width);
	app.m_Viewport.Height = static_cast<float>(height);

	float aspect = app.m_Swapchain.aspect();
	app.m_Camera->setAspect(aspect, aspect < 16.f / 9.f ? aspect / (16.f / 9.f) : 1.f);
}
