#pragma once

#include "stdafx.h"
#include "TerrainGenerator.h"

class MarchingCubesTerrainGenerator {
public:
	MarchingCubesTerrainGenerator(std::string param1);

	Mesh generateTerrain(const DXDevice& device);

private:
};