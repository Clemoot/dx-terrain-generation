#pragma once

#include "stdafx.h"
#include "DXTexture2D.h"

class DXShaderTexture : public DXTexture2D
{
public:
	DXShaderTexture();
	DXShaderTexture(const DXDevice& device, uint16_t width, uint16_t height);
	~DXShaderTexture();

	void createSampler(const DXDevice& device);

	const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState() const;
	const Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>& view() const;

private:
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_View;
	Microsoft::WRL::ComPtr<ID3D11SamplerState> m_SamplerState;
};

