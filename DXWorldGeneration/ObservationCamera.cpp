#include "stdafx.h"
#include "ObservationCamera.h"

ObservationCamera::ObservationCamera(std::string name):
	Camera(name),
	m_Angle(0.f),
	m_Distance(20.f),
	m_Height(20.f),
	m_LookAtOffset(10.f)
{
	m_Position = DirectX::XMFLOAT3(0.f, 0.7f, 1.5f);
}

ObservationCamera::~ObservationCamera()
{
}

void ObservationCamera::render_imgui()
{
	if (ImGui::CollapsingHeader("Camera")) {
		ImGui::SliderFloat("Angle", &m_Angle, 0, 360, "%.0f", 0);
		ImGui::SliderFloat("Distance", &m_Distance, 0, 50, "%.1f", 0);
		ImGui::SliderFloat("Height", &m_Height, 0, 50, "%.1f", 0);
		ImGui::SliderFloat("Offset", &m_LookAtOffset, 0, 50, "%.1f", 0);
	}
}

void ObservationCamera::update(const DXDevice& device)
{
	m_Position = DirectX::XMFLOAT3(m_Distance * cos(m_Angle / 180 * DirectX::XM_PI), m_Height, m_Distance * sin(m_Angle / 180 * DirectX::XM_PI));

	DirectX::XMFLOAT3 lookPos;
	DirectX::XMFLOAT3 lookOrigin = DirectX::XMFLOAT3(0, 0, 0);
	DirectX::XMStoreFloat3(
		&lookPos,
		DirectX::XMVectorAdd(
			DirectX::XMLoadFloat3(&lookOrigin),
			DirectX::XMVectorSet(0.f, m_LookAtOffset, 0.f, 0.f)
		)
	);
	lookAt(lookPos);

	SceneNode::update(device);
}
