Texture2D MainTexture : register(t0);
SamplerState SampleType;

struct PS_INPUT {
	float4 pos : SV_POSITION;
	float4 worldPos : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float4 color : COLOR;
};

float4 main(PS_INPUT i) : SV_TARGET
{
	return i.color;
	//return MainTexture.Sample(SampleType, i.uv);
}