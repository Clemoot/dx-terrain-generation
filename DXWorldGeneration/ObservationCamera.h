#pragma once

#include "stdafx.h"
#include "Camera.h"

class ObservationCamera : public Camera {
public:
	ObservationCamera(std::string name);
	~ObservationCamera();

	virtual void render_imgui();
	virtual void update(const DXDevice& device);

 private:
	 float m_Angle;
	 float m_Distance;
	 float m_Height;
	 float m_LookAtOffset;
};

