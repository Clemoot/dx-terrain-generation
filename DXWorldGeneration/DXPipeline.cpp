#include "stdafx.h"
#include "DXPipeline.h"
#include <d3dcompiler.h>
#include <d3d11shader.h>

DXPipeline::DXPipeline(const DXDevice& device, const std::string& vertexShader, const std::string& pixelShader, const std::string& geometryShader)
{
	unsigned char* bytecode;
	unsigned int bytecodeSize;

	if (!readShaderBytecode(vertexShader.c_str(), bytecode, bytecodeSize)) {
		LOG("failed to read vertex shader bytecode");
		return;
	}

	if (FAILED(device.device()->CreateVertexShader(bytecode, bytecodeSize, nullptr, m_VertexShader.GetAddressOf()))) {
		LOG("failed to create vertex shader");
		return;
	}

	ID3D11ShaderReflection* shaderReflection;
	if (FAILED(D3DReflect(bytecode, bytecodeSize, IID_PPV_ARGS(&shaderReflection)))) {
		LOG("failed to reflect vertex shader");
		delete[] bytecode;
		return;
	}


	D3D11_SHADER_DESC shaderDesc{};
	shaderReflection->GetDesc(&shaderDesc);

	D3D11_SIGNATURE_PARAMETER_DESC inputDesc{};
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputLayout;
	inputLayout.reserve(shaderDesc.InputParameters);
	for (uint32_t i = 0; i < shaderDesc.InputParameters; i++) {
		shaderReflection->GetInputParameterDesc(i, &inputDesc);

		D3D11_INPUT_ELEMENT_DESC element{};
		element.SemanticName = inputDesc.SemanticName;
		element.SemanticIndex = inputDesc.SemanticIndex;

		if (inputDesc.Mask == D3D_COMPONENT_MASK_X) {	// NbComponents = 1
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
				element.Format = DXGI_FORMAT_R32_FLOAT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
				element.Format = DXGI_FORMAT_R32_UINT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
				element.Format = DXGI_FORMAT_R32_SINT;
		}
		else if (inputDesc.Mask < D3D_COMPONENT_MASK_Z) {	// NbComponents = 2
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
				element.Format = DXGI_FORMAT_R32G32_FLOAT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
				element.Format = DXGI_FORMAT_R32G32_UINT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
				element.Format = DXGI_FORMAT_R32G32_SINT;
		}
		else if (inputDesc.Mask < D3D_COMPONENT_MASK_W) {	// NbComponents = 3
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
				element.Format = DXGI_FORMAT_R32G32B32_FLOAT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
				element.Format = DXGI_FORMAT_R32G32B32_UINT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
				element.Format = DXGI_FORMAT_R32G32B32_SINT;
		}
		else {	// inputDesc.Mask > D3D_COMPONENT_MASK_W	NbComponents = 4
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
				element.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
				element.Format = DXGI_FORMAT_R32G32B32A32_UINT;
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
				element.Format = DXGI_FORMAT_R32G32B32A32_SINT;
		}

		element.InputSlot = 0;
		element.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
		element.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		element.InstanceDataStepRate = 0;

		inputLayout.push_back(element);
	}

	if (FAILED(device.device()->CreateInputLayout(inputLayout.data(), inputLayout.size(), bytecode, bytecodeSize, &m_InputLayout))) {
		LOG("failed to create input layout");
		shaderReflection->Release();
		delete[] bytecode;
		return;
	}

	shaderReflection->Release();
	delete[] bytecode;

	if (!readShaderBytecode(pixelShader.c_str(), bytecode, bytecodeSize)) {
		LOG("failed to read pixel shader bytecode");
		return;
	}

	if (FAILED(device.device()->CreatePixelShader(bytecode, bytecodeSize, nullptr, m_PixelShader.GetAddressOf()))) {
		LOG("failed to create pixel shader");
		delete[] bytecode;
		return;
	}

	delete[] bytecode;

	if (!geometryShader.empty()) {
		if (!readShaderBytecode(geometryShader.c_str(), bytecode, bytecodeSize)) {
			LOG("failed to read geometry shader bytecode");
			return;
		}

		if (FAILED(device.device()->CreateGeometryShader(bytecode, bytecodeSize, nullptr, m_GeometryShader.GetAddressOf()))) {
			LOG("failed to create geomtry shader");
			delete[] bytecode;
			return;
		}

		delete[] bytecode;
	}

	CD3D11_BUFFER_DESC cbDesc(
		sizeof(ConstantBufferStruct),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D10_CPU_ACCESS_WRITE
	);

	if (FAILED(device.device()->CreateBuffer(&cbDesc, nullptr, m_ConstantBuffer.GetAddressOf()))) {
		LOG("failed to create constant buffer");
		return;
	}
}

DXPipeline::~DXPipeline()
{
	m_GeometryShader.Reset();
	m_PixelShader.Reset();
	m_ConstantBuffer.Reset();
	m_InputLayout.Reset();
	m_VertexShader.Reset();
}

void DXPipeline::load(const DXDevice& device) const
{

	D3D11_MAPPED_SUBRESOURCE mappedRes{};
	ZeroMemory(&mappedRes, sizeof(mappedRes));

	device.context()->Map(m_ConstantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRes);
	memcpy(mappedRes.pData, &m_ConstantBufferData, sizeof(ConstantBufferStruct));
	device.context()->Unmap(m_ConstantBuffer.Get(), 0);

	device.context()->VSSetShader(m_VertexShader.Get(), nullptr, 0);
	device.context()->IASetInputLayout(m_InputLayout.Get());
	device.context()->VSSetConstantBuffers(0, 1, m_ConstantBuffer.GetAddressOf());
	device.context()->PSSetShader(m_PixelShader.Get(), nullptr, 0);
	device.context()->PSSetConstantBuffers(0, 1, m_ConstantBuffer.GetAddressOf());
	device.context()->GSSetShader(m_GeometryShader.Get(), nullptr, 0);
}

ConstantBufferStruct& DXPipeline::constantBuffer()
{
	return m_ConstantBufferData;
}

bool DXPipeline::readShaderBytecode(const std::string path, unsigned char*& bytecode, unsigned int& bytecodeSize)
{
	FILE* file;
	fopen_s(&file, path.c_str(), "rb");

	if (file == nullptr)
		return false;

	fseek(file, 0, SEEK_END);
	bytecodeSize = ftell(file);
	fseek(file, 0, SEEK_SET);

	bytecode = new unsigned char[bytecodeSize];
	size_t bytesRead = fread_s(bytecode, bytecodeSize, 1, bytecodeSize, file);

	fclose(file);

	return true;
}
