#include "stdafx.h"
#include "Window.h"

HINSTANCE Window::sm_hInstance = nullptr;

Window::Window(const std::string windowName, const uint16_t width, const uint16_t height):
	m_hWnd(nullptr), m_Width(width), m_Height(height), m_OnResize(nullptr)
{
	if (sm_hInstance == nullptr)
		sm_hInstance = GetModuleHandle(nullptr);

	WNDCLASS wndClass{};
	wndClass.hInstance = sm_hInstance;
	wndClass.style = CS_DBLCLKS;
	wndClass.lpfnWndProc = wndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = sizeof(this);
	wndClass.hIcon = nullptr;
	wndClass.hCursor = LoadCursor(sm_hInstance, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = nullptr;
	wndClass.lpszClassName = WINDOW_CLASS_NAME;

	if (!RegisterClass(&wndClass)) {
		LOG("unable to register window class");
		return;
	}

	m_hWnd = CreateWindow(WINDOW_CLASS_NAME, StrToWStr(windowName).c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, nullptr, nullptr, sm_hInstance, 0);
	if (!m_hWnd) {
		LOG("failed to create window");
		return;
	}

	SetWindowLongPtr(m_hWnd, 0, (long)this);
}

Window::~Window() {
	if (m_hWnd)
		close();
}

void Window::show()
{
	ShowWindow(m_hWnd, SW_SHOW);
}

void Window::close() {
	if (!DestroyWindow(m_hWnd)) {
		LOG("failed to destroy window");
		return;
	}
	m_hWnd = nullptr;
	if (!UnregisterClass(WINDOW_CLASS_NAME, sm_hInstance)) {
		LOG("failed to unregister window class");
		return;
	}
}

WindowListener& Window::onResize()
{
	return m_OnResize;
}

const uint16_t& Window::width() const {
	return m_Width;
}

const uint16_t& Window::height() const {
	return m_Height;
}

const HWND& Window::handle() const {
	return m_hWnd;
}

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT Window::wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
		return true;

	Window* window = reinterpret_cast<Window*>(GetWindowLongPtr(hWnd, 0));
	switch (msg) {
	case WM_SIZE:
		if (window->m_OnResize)
			window->m_OnResize(hWnd, wParam, lParam);
		break;
	case WM_CLOSE:
		window->close();
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProcA(hWnd, msg, wParam, lParam);
}
