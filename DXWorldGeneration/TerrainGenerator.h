#pragma once

#include "stdafx.h"
#include "Mesh.h"

template <typename T>
concept TerrainGenerator = requires (T a, const DXDevice & device)
{
	{a.generateTerrain(device)} -> std::derived_from<Mesh>;
};