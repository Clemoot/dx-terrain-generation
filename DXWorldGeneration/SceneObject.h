#pragma once

#include "stdafx.h"
#include "SceneNode.h"
#include "Mesh.h"

class SceneObject : public SceneNode
{
public:
	SceneObject(std::string name);
	~SceneObject();

	void addMesh(const Mesh& mesh);
	virtual void render(const DXDevice& device, const DirectX::XMMATRIX& transform) const;

	const virtual std::vector<Mesh>& meshes() const;

protected:
	std::vector<Mesh> m_Meshes;
};

