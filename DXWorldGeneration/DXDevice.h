#pragma once

#include "stdafx.h"

class DXDevice {
public:
	DXDevice();
	~DXDevice();

	void dumpInfoQueue() const;

	const Microsoft::WRL::ComPtr<ID3D11Device>& device() const;
	const Microsoft::WRL::ComPtr<ID3D11DeviceContext>& context() const;
	const Microsoft::WRL::ComPtr<ID3D11InfoQueue>& infoQueue() const;
	const D3D_FEATURE_LEVEL& featureLevel() const;

private:
	Microsoft::WRL::ComPtr<IDXGIDevice> m_DXGIDevice;
	Microsoft::WRL::ComPtr<IDXGIAdapter> m_Adapter;
	Microsoft::WRL::ComPtr<ID3D11Device> m_Device;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_Context;
	Microsoft::WRL::ComPtr<ID3D11InfoQueue> m_InfoQueue;
	D3D_FEATURE_LEVEL m_FeatureLevel;
};