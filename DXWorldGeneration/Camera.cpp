#include "stdafx.h"
#include "Camera.h"

Camera::Camera(std::string name): 
	SceneNode(name),
	m_AspectX(16.f / 9.f),
	m_AspectY(1.f),
	m_Fov(70.f),
	m_Near(0.01f),
	m_Far(100.f)
{
}

Camera::~Camera()
{
}

DirectX::XMMATRIX Camera::getViewMatrix() const
{
	DirectX::XMMATRIX transform = getGlobalTransform();
	DirectX::XMVECTOR determinant = DirectX::XMMatrixDeterminant(transform);
	return DirectX::XMMatrixInverse(&determinant, transform);
}

DirectX::XMMATRIX Camera::getProjectionMatrix() const
{
	return DirectX::XMMatrixPerspectiveFovRH(
		2.f * std::atan(std::tan(DirectX::XMConvertToRadians(m_Fov) / 2.f) / m_AspectY),
		m_AspectX,
		m_Near,
		m_Far
		);
}

void Camera::lookAt(DirectX::XMFLOAT3 at)
{
	DirectX::XMFLOAT3 pos = globalPosition();

	/*
	DirectX::XMVECTOR direction =
		DirectX::XMVector3Normalize(
			DirectX::XMVectorSubtract(
				DirectX::XMLoadFloat3(&at),
				DirectX::XMLoadFloat3(&pos)
			)
		);

	DirectX::XMVECTOR front = DirectX::XMVectorSet(0.f, 0.f, -1.f, 0.f);

	DirectX::XMVECTOR rotAxis =
		DirectX::XMVector3Normalize(
			DirectX::XMVector3Cross(
				front,
				direction
			)
		);

	if (DirectX::XMVector3LengthSq(rotAxis).m128_f32[0] == 0)
		rotAxis = DirectX::XMVectorSet(0.f, 1.f, 0.f, 0.f);

	float dot = DirectX::XMVector3Dot(front, direction).m128_f32[0];
	float angle = std::acosf(dot);

	m_Rotation = DirectX::XMQuaternionRotationAxis(rotAxis, angle);
	*/

	if (pos.x == at.x && pos.y == at.y && pos.z == at.z) return;

	m_Rotation = DirectX::XMQuaternionRotationMatrix(
		DirectX::XMMatrixTranspose(
		DirectX::XMMatrixLookAtRH(
			DirectX::XMLoadFloat3(&pos), 
			DirectX::XMLoadFloat3(&at), 
			{ 0,1,0 })
		)
	);
}

void Camera::setAspect(const float& aspectX, const float& aspectY)
{
	m_AspectX = aspectX;
	m_AspectY = aspectY;
}

float& Camera::fov()
{
	return m_Fov;
}

float& Camera::near_plane()
{
	return m_Near;
}

float& Camera::far_plane()
{
	return m_Far;
}
