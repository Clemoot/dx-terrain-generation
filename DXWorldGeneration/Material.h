#pragma once

#include "stdafx.h"
#include "DXShaderTexture.h"
#include "DXPipeline.h"

class Material
{
public:
	Material(const DXDevice& device);
	~Material();

	void attach(const DXDevice& device) const;
	uint32_t createDedicatedBuffer(const DXDevice& device, uint32_t slot, size_t size);
	void setPipeline(const std::shared_ptr<DXPipeline>& pipeline);
	void setTexture(const std::shared_ptr<DXShaderTexture>& texture);
	void updateDedicatedBuffer(const DXDevice& device, uint32_t index, void* data);

	DirectX::XMFLOAT4& ambient();
	DirectX::XMFLOAT4& diffuse();
	DirectX::XMFLOAT4& specular();
	const std::shared_ptr<DXShaderTexture>& mainTexture() const;
	const Microsoft::WRL::ComPtr<ID3D11Buffer>& materialBuffer() const;
	const std::shared_ptr<DXPipeline>& pipeline() const;

private:
	__declspec(align(16))
	struct MaterialConstantBuffer {
		DirectX::XMFLOAT4 ambient;
		DirectX::XMFLOAT4 diffuse;
		DirectX::XMFLOAT4 specular;
	};

	struct DedicatedBuffer {
		size_t size = 0;
		uint32_t slot = 0;
		ID3D11Buffer* buffer;
	};

	std::vector<DedicatedBuffer> m_DedicatedBuffers;
	std::shared_ptr<DXShaderTexture> m_Texture;
	MaterialConstantBuffer m_MaterialData;
	Microsoft::WRL::ComPtr<ID3D11Buffer> m_MaterialBuffer;
	std::shared_ptr<DXPipeline> m_Pipeline;
};

