Texture2D MainTexture : register(t0);
SamplerState SampleType;

cbuffer Matrices : register(b0) {
	float4x4 world;
	float4x4 view;
	float4x4 projection;
	float4x4 mvp;
};

struct PS_INPUT {
	float4 pos : SV_POSITION;
	float4 worldPos : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float4 color : COLOR;
};

float4 main(PS_INPUT i) : SV_TARGET
{
	float3 lightDir = float3(0.0f, -1.0f, 0.0f);

	float d = dot(i.normal, -lightDir);
	d = clamp(d, 0.0f, 1.0f);

	float4 color = i.color;
	//float4 color = MainTexture.Sample(SampleType, i.uv);

	return color * (0.4f * d + 0.6f);
}