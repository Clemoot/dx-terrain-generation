#include "stdafx.h"
#include "DXTexture2D.h"

DXTexture2D::DXTexture2D() : m_Texture(nullptr), m_TextureDesc{}, m_TextureData(nullptr) {}

DXTexture2D::DXTexture2D(const DXDevice& device, const CD3D11_TEXTURE2D_DESC desc): m_Texture(nullptr), m_TextureDesc{}, m_TextureData(nullptr)
{
	if (FAILED(device.device()->CreateTexture2D(&desc, nullptr, m_Texture.GetAddressOf()))) {
		LOG("failed to create 2D texture");
		device.dumpInfoQueue();
		return;
	}
	m_Texture->GetDesc(&m_TextureDesc);
}

DXTexture2D::DXTexture2D(const Microsoft::WRL::ComPtr<ID3D11Texture2D>& texture): m_Texture(texture), m_TextureData(nullptr)
{
	m_Texture->GetDesc(&m_TextureDesc);
}

DXTexture2D::~DXTexture2D()
{
	m_Texture.Reset();
	if (m_TextureData) {
		for (uint32_t i = 0; i < m_TextureDesc.Width; i++)
			delete[] m_TextureData[i];
		delete[] m_TextureData;
	}
}

void DXTexture2D::createEmptyData()
{
	if (m_TextureData) return;
	m_TextureData = new uint8_t * [m_TextureDesc.Height];
	for (uint32_t i = 0; i < m_TextureDesc.Height; i++)
		m_TextureData[i] = new uint8_t[m_TextureDesc.Width * 4];
}

void DXTexture2D::retrieveFromGPU(const DXDevice& device)
{
	if (!m_TextureData)
		createEmptyData();

	D3D11_MAPPED_SUBRESOURCE mappedRes{};
	device.context()->Map(m_Texture.Get(), 0, D3D11_MAP_READ, 0, &mappedRes);
	// Read data
	device.context()->Unmap(m_Texture.Get(), 0);
}

void DXTexture2D::applyOnGPU(const DXDevice& device)
{
	if (!m_TextureData) return;

	D3D11_MAPPED_SUBRESOURCE mappedRes{};
	device.context()->Map(m_Texture.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRes);
	device.dumpInfoQueue();
	for (uint32_t i = 0; i < m_TextureDesc.Height; i++) {
		memcpy((uint8_t*)mappedRes.pData + mappedRes.RowPitch * i, m_TextureData[i], m_TextureDesc.Width * 4 * sizeof(uint8_t));
	}
	device.context()->Unmap(m_Texture.Get(), 0);
}

uint8_t* const* const DXTexture2D::data() const
{
	return m_TextureData;
}

const Microsoft::WRL::ComPtr<ID3D11Texture2D>& DXTexture2D::texture() const
{
	return m_Texture;
}

const D3D11_TEXTURE2D_DESC& DXTexture2D::texDesc() const
{
	return m_TextureDesc;
}
