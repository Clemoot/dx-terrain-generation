#include "stdafx.h"
#include "Material.h"

Material::Material(const DXDevice& device) :
	m_MaterialData{
		{1.f, 1.f, 1.f, 1.f},
		{0.5f, 0.5f, 0.5f, 1.f},
		{0.f, 0.f, 0.f, 1.f}
	},
	m_Texture(nullptr),
	m_Pipeline(nullptr)
{
	CD3D11_BUFFER_DESC desc(
		sizeof(MaterialConstantBuffer),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE
	);

	D3D11_SUBRESOURCE_DATA data{};
	data.pSysMem = &m_MaterialData;
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;

	if (FAILED(device.device()->CreateBuffer(&desc, &data, m_MaterialBuffer.GetAddressOf()))) {
		LOG("failed to create material constant buffer");
		device.dumpInfoQueue();
	}
}

Material::~Material()
{
	for (DedicatedBuffer& buffer : m_DedicatedBuffers)
		buffer.buffer->Release();
	m_MaterialBuffer.Reset();
}

DirectX::XMFLOAT4& Material::ambient()
{
	return m_MaterialData.ambient;
}

DirectX::XMFLOAT4& Material::diffuse()
{
	return m_MaterialData.diffuse;
}

DirectX::XMFLOAT4& Material::specular()
{
	return m_MaterialData.specular;
}

const std::shared_ptr<DXShaderTexture>& Material::mainTexture() const
{
	return m_Texture;
}

void Material::attach(const DXDevice& device) const
{
	if (!m_Pipeline) return;

	D3D11_MAPPED_SUBRESOURCE mappedRes{};
	ZeroMemory(&mappedRes, sizeof(mappedRes));

	device.context()->Map(m_MaterialBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRes);
	memcpy(mappedRes.pData, &m_MaterialData, sizeof(MaterialConstantBuffer));
	device.context()->Unmap(m_MaterialBuffer.Get(), 0);

	device.context()->VSSetConstantBuffers(1, 1, m_MaterialBuffer.GetAddressOf());

	if (m_Texture && m_Texture->view()) {
		device.context()->PSSetShaderResources(0, 1, m_Texture->view().GetAddressOf());
		device.context()->PSSetSamplers(0, 1, m_Texture->samplerState().GetAddressOf());
	}

	for (DedicatedBuffer buffer : m_DedicatedBuffers)
		device.context()->VSSetConstantBuffers(buffer.slot, 1, &buffer.buffer);

	m_Pipeline->load(device);

	device.dumpInfoQueue();
}

uint32_t Material::createDedicatedBuffer(const DXDevice& device, uint32_t slot, size_t size)
{
	DedicatedBuffer dedicatedBuffer;
	dedicatedBuffer.slot = slot;
	dedicatedBuffer.size = size;

	CD3D11_BUFFER_DESC desc(
		size,
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE
	);

	if (FAILED(device.device()->CreateBuffer(&desc, nullptr, &dedicatedBuffer.buffer))) {
		LOG("failed to create material constant buffer");
		device.dumpInfoQueue();
	}

	m_DedicatedBuffers.push_back(dedicatedBuffer);

	return static_cast<uint8_t>(m_DedicatedBuffers.size()) - 1;
}

void Material::setPipeline(const std::shared_ptr<DXPipeline>& pipeline)
{
	m_Pipeline = pipeline;
}

void Material::setTexture(const std::shared_ptr<DXShaderTexture>& texture)
{
	m_Texture = texture;
}


void Material::updateDedicatedBuffer(const DXDevice& device, uint32_t index, void* data)
{
	if (index >= m_DedicatedBuffers.size()) return;

	DedicatedBuffer& buffer = m_DedicatedBuffers[index];
	D3D11_MAPPED_SUBRESOURCE mappedRes{};
	ZeroMemory(&mappedRes, sizeof(mappedRes));
	device.context()->Map(buffer.buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRes);
	memcpy(mappedRes.pData, data, buffer.size);
	device.context()->Unmap(buffer.buffer, 0);
}

const Microsoft::WRL::ComPtr<ID3D11Buffer>& Material::materialBuffer() const
{
	return m_MaterialBuffer;
}

const std::shared_ptr<DXPipeline>& Material::pipeline() const
{
	return m_Pipeline;
}


