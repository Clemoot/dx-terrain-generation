#pragma once

#include "stdafx.h"
#include "DXDevice.h"

class DXTexture2D {
public:
	DXTexture2D();
	DXTexture2D(const DXDevice& device, const CD3D11_TEXTURE2D_DESC desc);
	DXTexture2D(const Microsoft::WRL::ComPtr<ID3D11Texture2D>& texture);
	~DXTexture2D();

	void createEmptyData();
	void fillRow(size_t row, uint8_t value);
	void fillTexture(uint8_t value);
	void retrieveFromGPU(const DXDevice& device);
	void applyOnGPU(const DXDevice& device);

	uint8_t* const * const data() const;
	const Microsoft::WRL::ComPtr<ID3D11Texture2D>& texture() const;
	const D3D11_TEXTURE2D_DESC& texDesc() const;

protected:
	uint8_t** m_TextureData;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> m_Texture;
	D3D11_TEXTURE2D_DESC m_TextureDesc;
};

inline void DXTexture2D::fillRow(size_t row, uint8_t value)
{
	if (!m_TextureData) return;
	if (row >= m_TextureDesc.Height) return;
	memset(m_TextureData[row], value, m_TextureDesc.Width * sizeof(uint8_t) * 4);
}

inline void DXTexture2D::fillTexture(uint8_t value)
{
	for (size_t i = 0; i < m_TextureDesc.Height; i++)
		fillRow(i, value);
}