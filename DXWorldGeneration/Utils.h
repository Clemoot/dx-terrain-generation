#pragma once

#include <iostream>
#include <string>

#define LOG(msg) log(__FUNCTION__, __LINE__, msg)

#define START_TIMER(name) std::chrono::steady_clock::time_point name = std::chrono::steady_clock::now()

#define TIMER_CHECKPOINT(output_name, timer_name) auto output_name = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - timer_name)

#define DISPLAY_TIMER_MICRO(text, timer) std::cout << text << ": " << static_cast<float>(timer.count()) / 1000.0f << " ms" << std::endl

void log(const std::string function, const unsigned int line, const std::string msg);

std::wstring StrToWStr(std::string str);

std::string WStrToStr(std::wstring str);