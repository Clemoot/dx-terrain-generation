
struct GS_INPUT {
	float4 pos : SV_POSITION;
	float4 worldPos : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float4 color : COLOR;
};

struct GS_OUTPUT {
	float4 pos : SV_POSITION;
	float4 worldPos : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float4 color : COLOR;
};

[maxvertexcount(3)]
void main(
	triangle GS_INPUT input[3] : SV_POSITION,
	inout TriangleStream< GS_OUTPUT > output
)
{
	float4 e1 = input[1].worldPos - input[0].worldPos;
	float4 e2 = input[2].worldPos - input[0].worldPos;

	float3 normal = normalize(cross(e2.xyz, e1.xyz));

	for (uint i = 0; i < 3; i++)
	{
		GS_OUTPUT o;
		o.pos = input[i].pos;
		o.worldPos = input[i].worldPos;
		o.normal = normal;
		o.uv = input[i].uv;
		o.color = input[i].color;
		output.Append(o);
	}
}