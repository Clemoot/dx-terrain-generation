#include "stdafx.h"
#include "DXSwapchain.h"

DXSwapchain::DXSwapchain(const Window& window, const DXDevice& device)
{
	DXGI_SWAP_CHAIN_DESC desc{};
	desc.BufferCount = 2;
	desc.BufferDesc.Width = window.width();
	desc.BufferDesc.Height = window.height();
	desc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	desc.BufferDesc.RefreshRate.Numerator = 60;
	desc.BufferDesc.RefreshRate.Denominator = 1;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.OutputWindow = window.handle();
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	desc.Windowed = TRUE;

	Microsoft::WRL::ComPtr<IDXGIDevice> dxgiDevice;
	device.device().As(&dxgiDevice);

	Microsoft::WRL::ComPtr<IDXGIAdapter> adapter;
	Microsoft::WRL::ComPtr<IDXGIFactory> factory;

	if (FAILED(dxgiDevice->GetAdapter(adapter.GetAddressOf()))) {
		LOG("failed to get device adapter");
		return;
	}

	adapter->GetParent(IID_PPV_ARGS(factory.GetAddressOf()));

	factory->CreateSwapChain(device.device().Get(), &desc, m_Swapchain.GetAddressOf());

	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
	m_Swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)texture.GetAddressOf());
	m_RenderTarget = DXRenderTexture(device, texture);
	m_DepthStencil = DXDepthStencilTexture(device, window.width(), window.height());
}

DXSwapchain::~DXSwapchain()
{
}

void DXSwapchain::clear(const DXDevice& device, const float teal[4], const float depth, const uint8_t stencil)
{
	device.context()->ClearRenderTargetView(m_RenderTarget.view().Get(), teal);
	device.context()->ClearDepthStencilView(m_DepthStencil.view().Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, depth, stencil);
}

void DXSwapchain::present() const
{
	m_Swapchain->Present(1, 0);
}

void DXSwapchain::resize(const DXDevice& device)
{
	m_RenderTarget.~DXRenderTexture();
	m_DepthStencil.~DXDepthStencilTexture();

	if (FAILED(m_Swapchain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0))) {
		LOG("failed to resize swapchain buffers");
		return;
	}

	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
	m_Swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)texture.GetAddressOf());
	m_RenderTarget = DXRenderTexture(device, texture);
	m_DepthStencil = DXDepthStencilTexture(device, m_RenderTarget.texDesc().Width, m_RenderTarget.texDesc().Height);
}

void DXSwapchain::setTarget(const DXDevice& device) const
{
	device.context()->OMSetRenderTargets(1, m_RenderTarget.view().GetAddressOf(), m_DepthStencil.view().Get());
}

const Microsoft::WRL::ComPtr<IDXGISwapChain>& DXSwapchain::swapchain() const
{
	return m_Swapchain;
}

const float DXSwapchain::aspect() const
{
	return static_cast<float>(m_RenderTarget.texDesc().Width) / static_cast<float>(m_RenderTarget.texDesc().Height);
}

const DXRenderTexture& DXSwapchain::renderTarget() const
{
	return m_RenderTarget;
}

const DXDepthStencilTexture& DXSwapchain::depthStencil() const
{
	return m_DepthStencil;
}
