#pragma once

#include "stdafx.h"

constexpr auto WINDOW_CLASS_NAME = L"DXWorldGenerationWindowClass";

typedef void(*WindowListener)(HWND, WPARAM, LPARAM);

class Window {
public:
	Window(const std::string windowName, const uint16_t width, const uint16_t height);
	~Window();

	void show();
	void close();

	WindowListener& onResize();

	const uint16_t& width() const;
	const uint16_t& height() const;
	const HWND& handle() const;

private:
	static LRESULT CALLBACK wndProc(HWND, UINT, WPARAM, LPARAM);

	static HINSTANCE sm_hInstance;

	HWND m_hWnd;
	uint16_t m_Width, m_Height;

	WindowListener m_OnResize;
};