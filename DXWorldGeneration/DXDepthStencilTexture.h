#pragma once

#include "stdafx.h"
#include "DXTexture2D.h"

class DXDepthStencilTexture: public DXTexture2D
{
public:
	DXDepthStencilTexture();
	DXDepthStencilTexture(const DXDevice& device, const uint16_t width, const uint16_t height);
	~DXDepthStencilTexture();

	const Microsoft::WRL::ComPtr<ID3D11DepthStencilView>& view() const;

private:
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_View;
};

