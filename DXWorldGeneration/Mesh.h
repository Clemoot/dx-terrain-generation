#pragma once

#include "stdafx.h"
#include "Material.h"

class Mesh {
public:
	static Mesh createPlane(const DXDevice& device, const uint16_t& length = 1);
	static Mesh createCube(const DXDevice& device);

	Mesh();
	~Mesh();

	void setVertices(const std::vector<DirectX::XMFLOAT3>& vertices);
	void setNormals(const std::vector<DirectX::XMFLOAT3>& normals);
	void setUVs(const std::vector<DirectX::XMFLOAT2>& uvs);
	void setColors(const std::vector<DirectX::XMFLOAT3>& colors);
	void setIndices(const std::vector<uint16_t>& indices);
	void setMaterial(const std::shared_ptr<Material>& material);

	void calculateNormals();
	void apply(const DXDevice& device);
	void attach(const DXDevice& device) const;
	void draw(const DXDevice& device) const;

	const std::shared_ptr<Material>& material() const;

private:

	struct VertexData {
		DirectX::XMFLOAT4 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 uv;
		DirectX::XMFLOAT4 color;
	};

	struct BufferState {
		uint32_t size;
		bool updateData;
		bool reallocateBuffers;
	};

	virtual void updateVertexBuffer(const DXDevice& device);
	void updateIndexBuffer (const DXDevice& device);

	std::shared_ptr<Material> m_Material;
	std::vector<DirectX::XMFLOAT3> m_Vertices;
	std::vector<DirectX::XMFLOAT3> m_Normals;
	std::vector<DirectX::XMFLOAT2> m_UVs;
	std::vector<DirectX::XMFLOAT3> m_Colors;
	std::vector<uint16_t> m_Indices;
	Microsoft::WRL::ComPtr<ID3D11Buffer> m_VertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer> m_IndexBuffer;
	BufferState m_VertexBufferState, m_IndexBufferState;
};