/**
* Inspired by https://www.redblobgames.com/maps/terrain-from-noise/
*/

#pragma once

#include "stdafx.h"
#include "SceneObject.h"

class PerlinTerrainObject : public SceneObject
{
public:
	PerlinTerrainObject(std::string name, const DXDevice& device);
	~PerlinTerrainObject();

	virtual void render_imgui();
	virtual void update(const DXDevice& device);

private:
	__declspec(align(16))
	struct TerrainShaderData {
		float frequency;
		float exponent;
	};

	TerrainShaderData m_TerrainData;
	uint8_t m_TerrainDataIndex;
};

