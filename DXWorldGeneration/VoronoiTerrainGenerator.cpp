#include "stdafx.h"
#include "VoronoiTerrainGenerator.h"
#include <algorithm>
#include <stack>

VoronoiTerrainGenerator::VoronoiTerrainGenerator(int32_t nbVoronoiSites, int32_t nbRelaxations):
	m_NbVoronoiSites(nbVoronoiSites),
	m_NbRelaxations(nbRelaxations)
{
}

VoronoiTerrainGenerator::VoronoiTerrainGenerator(std::tuple<int32_t, int32_t> params):
	m_NbVoronoiSites(std::get<0>(params)),
	m_NbRelaxations(std::get<1>(params))
{
}

Mesh VoronoiTerrainGenerator::generateTerrain(const DXDevice& device)
{
	std::cout << "\n>>>> Generate new terrain <<<<" << std::endl;

	START_TIMER(generateRandomPointsTimer);
	m_Points = generateRandomPoints(m_NbVoronoiSites, -1.f, -1.f, 1.f, 1.f);
	TIMER_CHECKPOINT(generateRandomPointsDuration, generateRandomPointsTimer);
	DISPLAY_TIMER_MICRO("Generate Random Points: ", generateRandomPointsDuration);

	START_TIMER(generateRelaxedDiagramTimer);
	auto diagram = generateRelaxedDiagram(m_NbRelaxations);
	TIMER_CHECKPOINT(generateRelaxedDiagramDuration, generateRelaxedDiagramTimer);
	DISPLAY_TIMER_MICRO("Generate Relaxed Diagram", generateRelaxedDiagramDuration);

	START_TIMER(generateIslandTimer);
	m_TerrainGraph = generateIsland(diagram);
	TIMER_CHECKPOINT(generateIslandDuration, generateIslandTimer);
	DISPLAY_TIMER_MICRO("Generate Island", generateIslandDuration);

	START_TIMER(setOceansAndCoastsTimer);
	setOceansAndCoasts(*m_TerrainGraph, IslandPred_Radial);
	TIMER_CHECKPOINT(setOceansAndCoastsDuration, setOceansAndCoastsTimer);
	DISPLAY_TIMER_MICRO("Set Oceans and Coasts", setOceansAndCoastsDuration);

	START_TIMER(setIslandHeightTimer);
	setIslandHeight(*m_TerrainGraph);
	TIMER_CHECKPOINT(setIslandHeightDuration, setIslandHeightTimer);
	DISPLAY_TIMER_MICRO("Set Island Height", setIslandHeightDuration);

	START_TIMER(generateDiagramMeshTimer);
	Mesh terrain = generateDiagramMesh(*m_TerrainGraph);
	TIMER_CHECKPOINT(generateDiagramMeshDuration, generateDiagramMeshTimer);
	DISPLAY_TIMER_MICRO("Generate Diagram Mesh", generateDiagramMeshDuration);

	terrain.apply(device);

	return terrain;
}

std::vector<mygal::Vector2<float>> VoronoiTerrainGenerator::generateRandomPoints(size_t nbPoints, float minX, float minY, float maxX, float maxY) const
{
	std::vector<mygal::Vector2<float>> points;
	points.reserve(nbPoints);
	float x, y;
	for (size_t i = 0; i < nbPoints; i++) {
		x = minX + (maxX - minX) * static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
		y = minY + (maxY - minY) * static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
		points.push_back(mygal::Vector2<float>(x, y));
	}
	return points;
}

mygal::Diagram<float> VoronoiTerrainGenerator::generateDiagram() const
{
	auto algorithm = mygal::FortuneAlgorithm<float>(m_Points);
	algorithm.construct();
	algorithm.bound(mygal::Box<float>{-1.05f, -1.05f, 1.05f, 1.05f}); // Take the bounding box slightly bigger than the intersection box
	auto diagram = algorithm.getDiagram();
	diagram.intersect(mygal::Box<float>{-1.0f, -1.0f, 1.0f, 1.0f});
	return diagram;
}

Mesh VoronoiTerrainGenerator::generateDiagramMesh(const TerrainGraph& terrain) const
{
	Mesh mesh;

	std::vector<DirectX::XMFLOAT3> vertices;
	std::vector<DirectX::XMFLOAT2> uvs;
	std::vector<DirectX::XMFLOAT3> colors;
	std::vector<uint16_t> indices;

	size_t nbVertices = terrain.corners.size() + terrain.chunks.size();
	vertices.reserve(nbVertices);
	uvs.reserve(nbVertices);
	colors.reserve(nbVertices);
	indices.reserve(terrain.chunks.size() * 3 * 4);	// suppose we have 4 triangles per chunk

	for (const TerrainCorner& corner : terrain.corners) {
		DirectX::XMFLOAT3 vertex = {
			corner.point.x,
			corner.height,
			corner.point.y
		};
		vertices.push_back(vertex);

		DirectX::XMFLOAT2 uv = {
			(corner.point.x + 1) / 2,
			(corner.point.y + 1) / 2
		};
		uvs.push_back(uv);

		Painter2D::Color color = paramsToColor(corner.biome, corner.height);
		colors.push_back(DirectX::XMFLOAT3(color.r / 255.f, color.g / 255.f, color.b / 255.f));
	}

	for (const TerrainChunk& chunk : terrain.chunks) {
		DirectX::XMFLOAT3 vertex = {
			chunk.point.x,
			chunk.height,
			chunk.point.y
		};
		size_t centerIndex = vertices.size();
		vertices.push_back(vertex);

		DirectX::XMFLOAT2 uv = {
			(chunk.point.x + 1) / 2,
			(chunk.point.y + 1) / 2
		};
		uvs.push_back(uv);

		for (size_t i = 0, nbCorners = chunk.corners.size(); i < nbCorners; i++) {
			uint16_t a = chunk.corners[i]->index;
			uint16_t b = chunk.corners[(i + 1) % nbCorners]->index;

			indices.push_back(a);
			indices.push_back(b);
			indices.push_back(static_cast<uint16_t>(centerIndex));
		}

		Painter2D::Color color = paramsToColor(chunk.biome, chunk.height);
		colors.push_back(DirectX::XMFLOAT3(color.r / 255.f, color.g / 255.f, color.b / 255.f));
	}

	auto tmpVector(vertices);
	auto end = std::unique(tmpVector.begin(), tmpVector.end(), [](DirectX::XMFLOAT3 a, DirectX::XMFLOAT3 b) { return abs(a.x - b.x) < 0.001f && abs(a.z == b.z) < 0.001f; });
	auto distance = std::distance(tmpVector.begin(), end);

	mesh.setVertices(vertices);
	mesh.setUVs(uvs);
	mesh.setColors(colors);
	mesh.setIndices(indices);
	mesh.calculateNormals();

	return mesh;
}

mygal::Diagram<float> VoronoiTerrainGenerator::generateRelaxedDiagram(size_t nbIterations)
{
	mygal::Diagram<float> diagram = generateDiagram();
	for (size_t i = 0; i < nbIterations; i++) {
		m_Points = diagram.computeLloydRelaxation();
		diagram = generateDiagram();
	}
	return diagram;
}

std::unique_ptr<VoronoiTerrainGenerator::TerrainGraph> VoronoiTerrainGenerator::generateIsland(const mygal::Diagram<float>& diagram) const
{
	std::vector<mygal::Diagram<float>::Site> sites = diagram.getSites();
	std::list<mygal::Diagram<float>::Vertex> vertices = diagram.getVertices();

	std::unique_ptr<TerrainGraph> graph = std::make_unique<TerrainGraph>();

	std::vector<TerrainChunk>& chunks = graph->chunks;
	chunks.reserve(sites.size());
	std::vector<TerrainCorner>& corners = graph->corners;
	corners.reserve(vertices.size());

	uint16_t index = 0;
	for (const auto& vertex : vertices) {
		if (findCorner(corners, vertex.point)) continue;

		TerrainCorner corner;
		corner.point = vertex.point;
		corner.biome = 0;
		corner.index = index++;
		corners.push_back(corner);
	}

	index = 0;
	for (const auto& site : sites) {
		TerrainChunk chunk;
		chunk.point = site.point;
		chunk.biome = 0;
		chunk.isBorder = false;
		chunk.index = index++;
		chunks.push_back(chunk);
	}

	for (size_t i = 0, nbChunks = chunks.size(); i < nbChunks; i++) {
		TerrainChunk& chunk = chunks[i];
		const auto& site = sites[i];

		size_t nb_edges = 0;
		auto halfEdge = site.face->outerComponent;
		if (halfEdge) {
			while (halfEdge->prev != nullptr) {
				if (halfEdge == site.face->outerComponent)
					break;
				halfEdge = halfEdge->prev;
				nb_edges++;
			}
			chunk.neighbours.reserve(nb_edges);
			chunk.corners.reserve(nb_edges);
			while (halfEdge != nullptr) {
				if (halfEdge->origin->point.x != halfEdge->destination->point.x
					|| halfEdge->origin->point.y != halfEdge->destination->point.y)
				{
					if (halfEdge->twin) {
						size_t index = halfEdge->twin->incidentFace->site->index;
						chunk.neighbours.push_back(chunks.data() + index);
					}
					else
						chunk.isBorder = true;
					TerrainCorner* originCorner = findCorner(corners, halfEdge->origin->point);
					originCorner->surroundings.push_back(&chunk);
					chunk.corners.push_back(originCorner);

					TerrainCorner* destCorner = findCorner(corners, halfEdge->destination->point);
					originCorner->neighbours.push_back(destCorner);
				}

				halfEdge = halfEdge->next;
				if (halfEdge == site.face->outerComponent)
					break;
			}
		}
	}

	return graph;
}

void VoronoiTerrainGenerator::setOceansAndCoasts(TerrainGraph& terrain, bool (*pred)(mygal::Vector2<float>, float))
{
	for (TerrainChunk& chunk : terrain.chunks)
		chunk.biome = (pred(chunk.point, 0.8f)) ? Biome::LAND : Biome::WATER;

	for (TerrainChunk& chunk : terrain.chunks) {
		if (!(chunk.biome & WATER)) continue;
		if (!chunk.isBorder) continue;
		if (chunk.biome & OCEAN) continue;

		std::stack<TerrainChunk*> chunksToExplore;
		chunksToExplore.push(&chunk);
		while (!chunksToExplore.empty()) {
			TerrainChunk* curChunk = chunksToExplore.top();
			chunksToExplore.pop();

			curChunk->biome |= OCEAN;

			for (TerrainChunk* neighbour : curChunk->neighbours) {
				if (neighbour->biome & LAND)
					neighbour->biome |= COAST;
				else if (neighbour->biome & WATER && !(neighbour->biome & OCEAN))
					chunksToExplore.push(neighbour);
			}
		}
	}

	for (TerrainCorner& corner : terrain.corners) {
		uint16_t nbLands = 0;
		uint16_t nbOceans = 0;
		for (TerrainChunk* chunk : corner.surroundings) {
			nbLands += (chunk->biome & LAND) > 0;
			nbOceans += (chunk->biome & OCEAN) > 0;
		}

		if (nbLands > 0) {
			corner.biome |= LAND;
			if (nbOceans > 0)
				corner.biome |= COAST | WATER;
		}
		else
			corner.biome |= OCEAN | WATER;
	}
}

void VoronoiTerrainGenerator::setIslandHeight(TerrainGraph& terrain)
{
	/**
	* La hauteur est assign�e en fonction de la distance entre le corner courant et la c�te la plus proche.
	* Pour assigner cette hauteur, pour chaque corner de la c�te, on ajoute dans une pile ses voisins s'ils font partie de l'�le.
	* A chaque passage, le corner retient la hauteur minimale. On peut arr�ter de parcourir les voisins quand la hauteur est plus petite que la hauteur courante du parcours.
	*/

	struct HeightTask {
		TerrainCorner* corner;	// current corner
		float currentHeight;	// current height
	};

	for (TerrainCorner& corner : terrain.corners) {
		// if corner is not a coast, do not start changing the height
		if (!(corner.biome & COAST)) {
			// if corner is in the ocean, override the default value (-1.0f)
			if (corner.biome & OCEAN)
				corner.height = 0.0f;
			continue;
		}

		std::stack<HeightTask> tasks;
		tasks.push({ &corner, 0.0f });	// initial state

		while (!tasks.empty()) {
			// retrieve a task
			HeightTask task = tasks.top();
			tasks.pop();

			// if corner height already set to a lower value, don't go further
			if (task.corner->height >= 0.0f && task.corner->height <= task.currentHeight)
				continue;

			// set corner height
			task.corner->height = task.currentHeight;

			// change current height to next height
			task.currentHeight++;

			// for each neighbour, add a new task if it is a land
			for (TerrainCorner* neighbour : task.corner->neighbours) {
				if (!(neighbour->biome & LAND)) continue;
				task.corner = neighbour;
				tasks.push(task);
			}
		}
	}

	std::vector<TerrainCorner> sortedCorners;
	sortedCorners.reserve(terrain.corners.size());
	for (TerrainCorner& corner : terrain.corners)
		if ((corner.biome & LAND) && !(corner.biome & COAST))
			sortedCorners.push_back(corner);
	std::sort(sortedCorners.begin(), sortedCorners.end(), [](TerrainCorner& c1, TerrainCorner& c2) { return c1.height < c2.height; });

	const float SCALE_FACTOR = 1.1f;

	for (size_t i = 0, nbCorners = sortedCorners.size(); i < nbCorners; i++) {
		float normalizedHeight = static_cast<float>(i) / static_cast<float>(nbCorners);

		float invSqrtHeight = std::sqrtf(SCALE_FACTOR) - std::sqrtf(SCALE_FACTOR * (1.f - normalizedHeight));

		size_t cornerIndex = sortedCorners[i].index;
		terrain.corners[cornerIndex].height = invSqrtHeight * normalizedHeight / 1.5f;
	}

	// set chunk height to the average height of its corners
	for (TerrainChunk& chunk : terrain.chunks) {
		float totalHeight = 0.0f;
		for (TerrainCorner* corner : chunk.corners)
			totalHeight += corner->height;
		chunk.height = totalHeight / static_cast<float>(chunk.neighbours.size());
	}
}

Painter2D::Color VoronoiTerrainGenerator::paramsToColor(uint16_t biome, float height)
{
	Painter2D::Color color = { 0U, 0U, 0U, 0U };
	if (biome & LAND) {
		color = { 168U, 112U, 63U, 255U };
		if (biome & COAST)
			color = { 255U, 183U, 76U, 255U };

		float normHeight = height / 1.1f;
		uint8_t rb = static_cast<uint8_t>(255.f * (0.9f * normHeight + 0.1f));
		uint8_t g = static_cast<uint8_t>(255.f * (0.7f * normHeight + 0.3f));

		color = {
			rb,
			g,
			rb,
			255U };
	}
	else if (biome & WATER) {
		color = { 51U, 102U, 153U, 255U };
		if (biome & OCEAN)
			color = { 54U, 54U, 97U, 255U };
	}

	return color;
}
/*
Painter2D::Point2D<int16_t> VoronoiTerrainGenerator::pointToTexcoord(mygal::Vector2<float> point) const
{
	const auto& desc = m_Meshes[0].material()->mainTexture()->texDesc();
	float x = (desc.Width - 3) * (point.x + 1) / 2 + 1;
	float y = (desc.Height - 3) * (point.y + 1) / 2 + 1;
	return Painter2D::Point2D<int16_t>{
		static_cast<int16_t>(x),
			static_cast<int16_t>(y)
	};
}

std::vector<Painter2D::Point2D<int16_t>> VoronoiTerrainGenerator::cornersToTexcoord(const std::vector<TerrainCorner*> corners) const
{
	std::vector<Painter2D::Point2D<int16_t>> points;
	points.reserve(corners.size());
	for (const auto& corner : corners)
		points.push_back(pointToTexcoord(corner->point));
	return points;
}

void VoronoiTerrainGenerator::drawDiagramOnTexture(const DXDevice& device, const mygal::Diagram<float>& diagram)
{
	auto texture = m_Meshes[0].material()->mainTexture();
	texture->fillTexture(0U);
	const auto& texDesc = texture->texDesc();
	Painter2D painter(texture->data(), texDesc.Width, texDesc.Height);

	for (const auto& site : diagram.getSites())
	{
		auto center = site.point;
		auto face = site.face;
		auto halfEdge = face->outerComponent;
		if (halfEdge == nullptr)
			continue;
		while (halfEdge->prev != nullptr)
		{
			halfEdge = halfEdge->prev;
			if (halfEdge == face->outerComponent)
				break;
		}
		auto startHalfEdge = halfEdge;
		while (halfEdge != nullptr)
		{
			if (halfEdge->origin != nullptr && halfEdge->destination != nullptr)
				painter.drawLine(
					pointToTexcoord(halfEdge->origin->point),
					pointToTexcoord(halfEdge->destination->point)
				);
			halfEdge = halfEdge->next;
			if (halfEdge == startHalfEdge)
				break;
		}
	}

	for (const auto& point : m_Points)
		painter.drawPoint(
			pointToTexcoord(point),
			{ 255U, 0U, 0U, 255U }
	);

	texture->applyOnGPU(device);
}

void VoronoiTerrainGenerator::drawChunksOnTexture(const DXDevice& device, const std::vector<TerrainChunk>& chunks)
{
	auto texture = m_Meshes[0].material()->mainTexture();
	texture->fillTexture(0U);
	const auto& texDesc = texture->texDesc();
	Painter2D painter(texture->data(), texDesc.Width, texDesc.Height);

	for (const auto& chunk : chunks)
	{
		Painter2D::Color color;
		if (chunk.biome & WATER) {
			color = { 64U, 64U, 255U, 255U };
			if (chunk.biome & OCEAN)
				color = { 0U, 0U, 255U, 255U };
		}
		else if (chunk.biome & LAND) {
			color = { 168U, 112U, 63U, 255U };
			if (chunk.biome & COAST)
				color = { 255U, 183U, 76U, 255U };
		}
		else
			color = { 0U, 0U, 0U, 0U };

		auto points = cornersToTexcoord(chunk.corners);
		painter.drawFace(points, color);

		for (size_t i = 0, nbCorners = chunk.corners.size(); i < nbCorners; i++)
			painter.drawLine(
				pointToTexcoord(chunk.corners[i]->point),
				pointToTexcoord(chunk.corners[(i + 1) % nbCorners]->point)
			);

		painter.drawPoint(
			pointToTexcoord(chunk.point),
			color
		);
	}

	texture->applyOnGPU(device);
}
*/
VoronoiTerrainGenerator::TerrainChunk* VoronoiTerrainGenerator::findChunk(std::vector<TerrainChunk>& chunks, const mygal::Vector2<float>& point)
{
	auto it = std::find_if(chunks.begin(), chunks.end(),
		[&](const TerrainChunk& c) {
			return c.point.x == point.x && c.point.y == point.y;
		});
	if (it == chunks.end())
		return nullptr;
	return chunks.data() + std::distance(chunks.begin(), it);
}

VoronoiTerrainGenerator::TerrainCorner* VoronoiTerrainGenerator::findCorner(std::vector<TerrainCorner>& corners, const mygal::Vector2<float>& point)
{
	auto it = std::find_if(corners.begin(), corners.end(),
		[&](const TerrainCorner& c) {
			return c.point.x == point.x && c.point.y == point.y;
		});
	if (it == corners.end())
		return nullptr;
	return corners.data() + std::distance(corners.begin(), it);
}

bool VoronoiTerrainGenerator::IslandPred_Radial(mygal::Vector2<float> point, float threshold)
{
	return point.getNorm() < threshold;
}
