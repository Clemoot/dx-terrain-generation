#pragma once

#include "stdafx.h"
#include "DXDevice.h"

class SceneNode
{
public:
	SceneNode(std::string name);
	SceneNode(DirectX::XMFLOAT3 pos, DirectX::XMVECTOR rot, DirectX::XMFLOAT3 scale);
	virtual ~SceneNode();

	void addChild(SceneNode* const child);
	void disable();
	void draw(const DXDevice& device) const;
	void draw(const DXDevice& device, DirectX::XMMATRIX* transform) const;
	void draw_imgui();
	void enable();
	DirectX::XMMATRIX getLocalTransform() const;
	DirectX::XMMATRIX getGlobalTransform() const;
	virtual void render(const DXDevice& device, const DirectX::XMMATRIX& transform) const;
	virtual void render_imgui();
	virtual void update(const DXDevice& device);
	void setParent(SceneNode* const parent);

	DirectX::XMFLOAT3 globalPosition() const;
	DirectX::XMFLOAT3& position();
	DirectX::XMVECTOR& rotation();
	DirectX::XMFLOAT3& scale();

	void* operator new(size_t i) {
		return _aligned_malloc(i, 16);
	}

	void operator delete(void* p) {
		_aligned_free(p);
	}

	static void deleteNodeAndChildren(SceneNode* node);

protected:
	std::string m_Name;
	bool m_Enabled;

	DirectX::XMFLOAT3 m_Position;
	DirectX::XMVECTOR m_Rotation;
	DirectX::XMFLOAT3 m_Scale;

	SceneNode* m_Parent;
	std::vector<SceneNode*> m_Children;
};

