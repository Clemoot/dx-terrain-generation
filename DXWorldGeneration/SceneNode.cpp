#include "stdafx.h"
#include "SceneNode.h"

SceneNode::SceneNode(std::string name) :
	m_Position(0, 0, 0),
	m_Rotation(DirectX::XMQuaternionIdentity()),
	m_Scale(1.f, 1.f, 1.f),
	m_Parent(),
	m_Name(name),
	m_Enabled(true)
{
}

SceneNode::SceneNode(DirectX::XMFLOAT3 pos, DirectX::XMVECTOR rot, DirectX::XMFLOAT3 scale) :
	m_Position(pos),
	m_Rotation(rot),
	m_Scale(scale),
	m_Parent(),
	m_Enabled(true)
{
}

SceneNode::~SceneNode()
{
	size_t nbChildren = m_Children.size();
	for (size_t i = 0; i < nbChildren; i++)
		m_Children[i]->m_Parent = m_Parent;

	if (m_Parent) {
		auto end = m_Parent->m_Children.end();
		for (auto it = m_Parent->m_Children.begin(); it != end; it++) {
			if (*it == this) {
				m_Parent->m_Children.erase(it);
				break;
			}
		}
	}
}

void SceneNode::addChild(SceneNode* const child)
{
	if (!child) return;
	if (child->m_Parent == this) return;
	child->m_Parent = this;
	m_Children.push_back(child);
}

void SceneNode::disable()
{
	m_Enabled = false;
}

void SceneNode::draw(const DXDevice& device) const
{
	draw(device, nullptr);
}

void SceneNode::draw(const DXDevice& device, DirectX::XMMATRIX* transform) const
{
	if (!m_Enabled) return;

	DirectX::XMMATRIX t = getLocalTransform();
	if (!transform)
		transform = &t;
	else
		*transform = DirectX::XMMatrixMultiply(t, *transform);

	render(device, *transform);

	size_t nbChildren = m_Children.size();
	for (size_t i = 0; i < nbChildren; i++)
		m_Children[i]->draw(device, transform);
}

void SceneNode::draw_imgui()
{
	if (!m_Enabled) return;

	render_imgui();

	size_t nbChildren = m_Children.size();
	for (size_t i = 0; i < nbChildren; i++)
		m_Children[i]->draw_imgui();
}

void SceneNode::enable()
{
	m_Enabled = true;
}

DirectX::XMMATRIX SceneNode::getLocalTransform() const
{
	DirectX::XMMATRIX translation = DirectX::XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);
	DirectX::XMMATRIX rotation = DirectX::XMMatrixRotationQuaternion(m_Rotation);
	DirectX::XMMATRIX scale = DirectX::XMMatrixScaling(m_Scale.x, m_Scale.y, m_Scale.z);
	DirectX::XMMATRIX transform = DirectX::XMMatrixMultiply(
		scale,
		DirectX::XMMatrixMultiply(
			rotation,
			translation
		)
	);

	return transform;
}

DirectX::XMMATRIX SceneNode::getGlobalTransform() const
{
	if (!m_Parent) {
		return getLocalTransform();
	}

	SceneNode* curNode = m_Parent;
	DirectX::XMMATRIX transform = getLocalTransform();
	while (curNode) {
		transform = DirectX::XMMatrixMultiply(
			curNode->getLocalTransform(),
			transform
		);
		curNode = curNode->m_Parent;
	}

	return transform;
}

void SceneNode::render(const DXDevice& device, const DirectX::XMMATRIX& transform) const
{
}

void SceneNode::render_imgui()
{
}

void SceneNode::update(const DXDevice& device)
{
	size_t nbChildren = m_Children.size();
	for (size_t i = 0; i < nbChildren; i++)
		m_Children[i]->update(device);
}

void SceneNode::setParent(SceneNode* const parent)
{
	if (m_Parent == parent) return;
	m_Parent = parent;
	parent->m_Children.push_back(this);
}

DirectX::XMFLOAT3 SceneNode::globalPosition() const
{
	DirectX::XMMATRIX transform = getGlobalTransform();
	DirectX::XMFLOAT3 globalPosition(transform.r[3].m128_f32);
	return globalPosition;
}

DirectX::XMFLOAT3& SceneNode::position()
{
	return m_Position;
}

DirectX::XMVECTOR& SceneNode::rotation()
{
	return m_Rotation;
}

DirectX::XMFLOAT3& SceneNode::scale()
{
	return m_Scale;
}

void SceneNode::deleteNodeAndChildren(SceneNode* node)
{
	if (!node) return;
	if (node->m_Children.size()) {
		for (size_t i = node->m_Children.size() - 1; i > 0; i--)
			deleteNodeAndChildren(node->m_Children[i]);
		deleteNodeAndChildren(node->m_Children[0]);
	}
	delete node;
}
