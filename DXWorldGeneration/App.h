#pragma once

#include "stdafx.h"

#include "DXDevice.h"
#include "Window.h"
#include "DXSwapchain.h"

#include "Mesh.h"
#include "SceneObject.h"
#include "ObservationCamera.h"
#include "TerrainContainer.h"
#include "PerlinTerrainObject.h"
#include "GenerativeTerrain.h"
#include "VoronoiTerrainGenerator.h"
#include "MarchingCubesTerrainGenerator.h"

class App {
public:
	static App* Instance;

	App();
	~App();

	void run();
	DXDevice& device() { return m_Device; }

	static void OnWindowResize(HWND hWnd, WPARAM wParam, LPARAM lParam);

private:
	SceneNode* m_SceneRoot;
	SceneObject* m_Objects[3];
	Camera* m_Camera;

	Window m_Window;
	DXDevice m_Device;
	DXSwapchain m_Swapchain;

	D3D11_VIEWPORT m_Viewport;

	int m_SelectedObjectIndex;

};