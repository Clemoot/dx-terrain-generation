#include "stdafx.h"
#include "DXShaderTexture.h"

DXShaderTexture::DXShaderTexture() :
	m_View(nullptr),
	m_SamplerState(nullptr)
{
}

DXShaderTexture::DXShaderTexture(const DXDevice& device, uint16_t width, uint16_t height) :
	DXTexture2D(device,
		CD3D11_TEXTURE2D_DESC(
			DXGI_FORMAT_R8G8B8A8_UNORM,
			static_cast<uint32_t>(width),
			static_cast<uint32_t>(height),
			1,
			1,
			D3D11_BIND_SHADER_RESOURCE,
			D3D11_USAGE_DYNAMIC,
			D3D11_CPU_ACCESS_WRITE
		)
	),
	m_SamplerState(nullptr)
{
	D3D11_SHADER_RESOURCE_VIEW_DESC desc{};
	ZeroMemory(&desc, sizeof(desc));
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	desc.Texture2D.MipLevels = 1;

	if (FAILED(device.device()->CreateShaderResourceView(m_Texture.Get(), &desc, m_View.GetAddressOf()))) {
		LOG("failed to create shader resource view");
		device.dumpInfoQueue();
		return;
	}
}

DXShaderTexture::~DXShaderTexture()
{
	m_View.Reset();
}

void DXShaderTexture::createSampler(const DXDevice& device)
{
	D3D11_SAMPLER_DESC desc{};
	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.MipLODBias = 0.0f;
	desc.MaxAnisotropy = 1;
	desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	desc.BorderColor[0] = 0.f;
	desc.BorderColor[1] = 0.f;
	desc.BorderColor[2] = 0.f;
	desc.BorderColor[3] = 0.f;
	desc.MinLOD = 0;
	desc.MaxLOD = D3D11_FLOAT32_MAX;

	if (FAILED(device.device()->CreateSamplerState(&desc, m_SamplerState.GetAddressOf()))) {
		LOG("failed to create sampler state");
		device.dumpInfoQueue();
		return;
	}
}

const Microsoft::WRL::ComPtr<ID3D11SamplerState>& DXShaderTexture::samplerState() const
{
	return m_SamplerState;
}

const Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>& DXShaderTexture::view() const
{
	return m_View;
}
