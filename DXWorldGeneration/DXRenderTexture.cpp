#include "stdafx.h"
#include "DXRenderTexture.h"

DXRenderTexture::DXRenderTexture(): DXTexture2D(), m_View(nullptr) {}

DXRenderTexture::DXRenderTexture(const DXDevice& device, const Microsoft::WRL::ComPtr<ID3D11Texture2D>& texture): DXTexture2D(texture)
{
	device.device()->CreateRenderTargetView(texture.Get(), nullptr, m_View.GetAddressOf());
}

DXRenderTexture::~DXRenderTexture()
{
	m_View.Reset();
}

const Microsoft::WRL::ComPtr<ID3D11RenderTargetView>& DXRenderTexture::view() const
{
	return m_View;
}