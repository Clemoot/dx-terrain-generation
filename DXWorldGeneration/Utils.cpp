#include "stdafx.h"
#include "Utils.h"

void log(const std::string function, const unsigned int line, const std::string msg) {
	std::cout << "Function " << function << " at " << line << " : " << msg << std::endl;
}

std::wstring StrToWStr(std::string str) {
	return std::wstring(str.begin(), str.end());
}

std::string WStrToStr(std::wstring str)
{
	return std::string(str.begin(), str.end());
}
