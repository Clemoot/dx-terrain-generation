#pragma once

#include <tuple>
#include "SceneObject.h"
#include "VoronoiTerrainGenerator.h"


template<typename T, size_t N>
struct GenerativeTerrainParam {
	T min;
	T max;
	char desc[N];

	constexpr GenerativeTerrainParam(const char(& str)[N], T m, T M) :
		min(m),
		max(M)
	{
		std::copy_n(str, N, desc);
	}
};

template<GenerativeTerrainParam... _Params>
struct GenerativeTerrainParams {
	static auto&& render_imgui_type(int index) {
		return _render_imgui_type(index, _Params...);
	}

	template<typename T>
	static auto&& _render_imgui_type(T& cur) {
		return cur;
	}

	template<typename T>
	static auto&& _render_imgui_type(int index, T& cur) {
		return cur;
	}

	template<typename T, typename... _Rest>
	static auto&& _render_imgui_type(int index, T& cur, _Rest&&... rest) {
		if (index == 0)
			return _render_imgui_type(cur);
		_render_imgui_type(index - 1, rest...);
	}
};

template<TerrainGenerator _Generator, typename _Params, typename... _Args >
class GenerativeTerrain : public SceneObject {
public:
	GenerativeTerrain(std::string name, const DXDevice& device) :
		SceneObject(name),
		m_GenParams()
	{
		_construct(device);
	}

	GenerativeTerrain(std::string name, const DXDevice& device, const _Args&&... args) :
		SceneObject(name),
		m_GenParams(args)
	{
		_construct(device);
	}

	GenerativeTerrain(std::string name, const DXDevice& device, const std::tuple<_Args...> args) :
		SceneObject(name),
		m_GenParams(args)
	{
		_construct(device);
	}

	virtual ~GenerativeTerrain() {

	}

	virtual void render_imgui() {
		render_imgui_template();
	}

	template<size_t I = 0>
	typename std::enable_if<(I == sizeof...(_Args)), void>::type
		render_imgui_template() {}

	template<size_t I = 0>
	typename std::enable_if<(I < sizeof...(_Args)), void>::type 
		render_imgui_template()
	{
		auto&& paramInfo = _Params::render_imgui_type(I);
		ImGui::SliderInt(paramInfo.desc,
			&std::get<I>(m_GenParams),
			paramInfo.min,
			paramInfo.max);
		render_imgui_template<I + 1>();
	}

	const std::tuple<_Args...>& genParams() const {
		return m_GenParams;
	}

private:
	std::tuple<_Args...> m_GenParams;

	void _construct(const DXDevice& device) {
		const std::string vertexShader = "BasicVS.cso";
		const std::string fragmentShader = "VertexColorAndLightPS.cso";
		const std::string geometryShader = "FaceNormal.cso";
		auto material = std::make_shared<Material>(device);
		material->ambient() = DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
		material->setPipeline(std::make_shared<DXPipeline>(device, vertexShader, fragmentShader, geometryShader));

		auto texture = std::make_shared<DXShaderTexture>(device, 1024, 1024);
		texture->createSampler(device);
		texture->createEmptyData();
		material->setTexture(texture);

		_Generator generator(m_GenParams);
		addMesh(generator.generateTerrain(device));
		m_Meshes[0].setMaterial(material);

		m_Scale = DirectX::XMFLOAT3(15, 15, 15);
	}
};

using VoronoiTerrain = GenerativeTerrain < VoronoiTerrainGenerator,
	GenerativeTerrainParams<
		GenerativeTerrainParam("Number of Voronoi points", 20, 2000),
		GenerativeTerrainParam("Number of relaxations", 0, 4)
	>,
	int32_t, int32_t >;