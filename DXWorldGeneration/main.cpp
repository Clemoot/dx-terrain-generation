#include "stdafx.h"
#include "App.h"

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "DXGI.lib")
#pragma comment (lib, "dxguid.lib")
#pragma comment (lib, "d3dcompiler.lib")

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include <dxgidebug.h>

int main(int argc, char** argv) {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	srand(static_cast<unsigned int>(time(0)));
	Microsoft::WRL::ComPtr<IDXGIDebug> debug;
	typedef HRESULT(WINAPI* LPDXGIGETDEBUGINTERFACE)(REFIID, void**);

	HMODULE dxgiDebug = LoadLibraryEx(L"dxgidebug.dll", nullptr, LOAD_LIBRARY_SEARCH_SYSTEM32);
	if (dxgiDebug) {
		auto dxgiGetDebugInterface = reinterpret_cast<LPDXGIGETDEBUGINTERFACE>(reinterpret_cast<void*>(GetProcAddress(dxgiDebug, "DXGIGetDebugInterface")));

		if (FAILED(dxgiGetDebugInterface(IID_PPV_ARGS(debug.GetAddressOf())))) {
			LOG("failed to get DXGI debug interface");
			return 1;
		}
	}
	else {
		LOG("failed to load dxgidebug.dll");
		return 1;
	}

	{
		App app;
		app.run();
		app.device().dumpInfoQueue();
	}

	debug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_DETAIL);
	//_CrtDumpMemoryLeaks();
	return 0;
}