#include "stdafx.h"
#include "DXDevice.h"

DXDevice::DXDevice()
{
	const D3D_FEATURE_LEVEL levels[] = {
		// D3D_FEATURE_LEVEL_9_1,
		// D3D_FEATURE_LEVEL_9_2,
		// D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_11_1,
	};

	Microsoft::WRL::ComPtr<IDXGIFactory> factory;
	if (FAILED(CreateDXGIFactory(IID_PPV_ARGS(&factory)))) {
		LOG("failed to create DXGI factory");
	}

	size_t nbLevels = sizeof(levels) / sizeof(levels[0]);

	uint32_t deviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#ifdef _DEBUG
	deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	if (FAILED(D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, 0, deviceFlags, levels, nbLevels, D3D11_SDK_VERSION, m_Device.GetAddressOf(), &m_FeatureLevel, m_Context.GetAddressOf()))) {
		LOG("failed to create device");
		return;
	}

	if (FAILED(m_Device->QueryInterface(IID_PPV_ARGS(m_InfoQueue.GetAddressOf())))) {
		LOG("failed to retrieve info queue");
	}

	if (FAILED(m_Device->QueryInterface(IID_PPV_ARGS(m_DXGIDevice.GetAddressOf())))) {
		LOG("failed to retrieve DXGI device");
	}
	m_DXGIDevice->GetAdapter(m_Adapter.GetAddressOf());

	DXGI_ADAPTER_DESC desc{};
	m_Adapter->GetDesc(&desc);
	std::cout << "Adapter used : " << WStrToStr(desc.Description) << std::endl;
}

DXDevice::~DXDevice()
{
	m_Adapter.Reset();
	m_DXGIDevice.Reset();
	m_Context->Flush();
	m_Context.Reset();
	m_Device.Reset();
}

void DXDevice::dumpInfoQueue() const
{
	uint64_t nbMessages = m_InfoQueue->GetNumStoredMessages();
	for (uint64_t i = 0; i < nbMessages; i++) {
		SIZE_T messageLength = 0;
		m_InfoQueue->GetMessageW(i, nullptr, &messageLength);

		D3D11_MESSAGE* message = (D3D11_MESSAGE*)malloc(messageLength);
		if (!message) break;
		m_InfoQueue->GetMessageW(i, message, &messageLength);
		std::cout << "[" << message->Category << "][" << message->Severity << "]" << message->ID << ":" << message->pDescription << std::endl;
		free(message);
	}
	m_InfoQueue->ClearStoredMessages();
}

const Microsoft::WRL::ComPtr<ID3D11Device>& DXDevice::device() const {
	return m_Device;
}

const Microsoft::WRL::ComPtr<ID3D11DeviceContext>& DXDevice::context() const {
	return m_Context;
}

const Microsoft::WRL::ComPtr<ID3D11InfoQueue>& DXDevice::infoQueue() const
{
	return m_InfoQueue;
}

const D3D_FEATURE_LEVEL& DXDevice::featureLevel() const {
	return m_FeatureLevel;
}