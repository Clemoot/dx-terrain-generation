#pragma once

#include "stdafx.h"
#include "DXTexture2D.h"

class DXRenderTexture : public DXTexture2D {
public:
	DXRenderTexture();
	DXRenderTexture(const DXDevice& device, const Microsoft::WRL::ComPtr<ID3D11Texture2D>& texture);
	~DXRenderTexture();

	const Microsoft::WRL::ComPtr<ID3D11RenderTargetView>& view() const;

private:
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_View;
};