cbuffer Matrices : register(b0) {
	float4x4 world;
	float4x4 view;
	float4x4 projection;
	float4x4 mvp;
};

cbuffer Material : register(b1) {
	float4 ambient;
	float4 diffuse;
	float4 specular;
};

cbuffer TerrainData : register(b2) {
    float frequency;
    float exponent;
}

struct VS_INPUT {
	float4 pos : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
	float4 color : COLOR;
};

struct VS_OUTPUT {
	float4 pos : SV_POSITION;
    float4 worldPos : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
	float4 color : COLOR;
};

/* Create pseudorandom direction vector
 */
float2 randomGradient(int2 i) {
    // No precomputed gradients mean this works for any number of grid coordinates
     int w = 8;
     int s = w / 2; // rotation width
     int a = i.x, b = i.y;
     a *= 3284157443; b ^= a << s | a >> w - s;
     b *= 1911520717; a ^= b << s | b >> w - s;
     a *= 2048419325;
     float random = a * (3.14159265 / ~(~0u >> 1)); // in [0, 2*Pi]
    //float random = frac(sin(dot(i, float2(12.9898, 78.233))) * 43758.5453);
    float2 v;
    v.x = sin(random); v.y = cos(random);
    return v;
}

// Computes the dot product of the distance and gradient vectors.
float dotGridGradient(int2 i, float2 uv) {
    // Get gradient from integer coordinates
    float2 gradient = randomGradient(i);

    // Compute the distance vector
    float dx = uv.x - (float)i.x;
    float dy = uv.y - (float)i.y;

    // Compute the dot-product
    return (dx * gradient.x + dy * gradient.y);
}

// Compute Perlin noise at coordinates x, y
float perlin(float2 uv) {
    // Determine grid cell coordinates
    int2 a = (int2)uv;
    int2 b = a + float2(1, 0);
    int2 c = a + float2(0, 1);
    int2 d = a + float2(1, 1);

    // Determine interpolation weights
    // Could also use higher order polynomial/s-curve here
    float2 s = uv - (float2)a;

    // Interpolate between grid point gradients
    float n0, n1, ix0, ix1, value;

    n0 = dotGridGradient(a, uv);
    n1 = dotGridGradient(b, uv);
    ix0 = lerp(n0, n1, s.x);

    n0 = dotGridGradient(c, uv);
    n1 = dotGridGradient(d, uv);
    ix1 = lerp(n0, n1, s.x);

    value = lerp(ix0, ix1, s.y);
    return (value + 1) / 2;
}

VS_OUTPUT main(VS_INPUT i)
{
	VS_OUTPUT o;
	//o.pos = mul(mul(mul(i.pos,world),view),projection);

    const float2 noise_input = i.uv;

    float e =
          1.0f * perlin(1 * frequency * noise_input)
        + 0.5f * perlin(2 * frequency * noise_input)
        + 0.25f * perlin(4 * frequency * noise_input)
        ;
    e /= (1.f + 0.5f + 0.25f);
    float elevation = pow(e * 1.2f, exponent);
    float4 objPos = i.pos +float4(0, elevation, 0, 0);
    o.worldPos = mul(objPos, world);
	o.pos = mul(objPos, mvp);
    o.normal = i.normal;
	o.uv = i.uv;
    o.color = e;

	return o;
}