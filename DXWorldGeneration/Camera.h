#pragma once

#include "stdafx.h"
#include "SceneNode.h"

class Camera : public SceneNode
{
public:
	Camera(std::string name);
	~Camera();

	DirectX::XMMATRIX getViewMatrix() const;
	DirectX::XMMATRIX getProjectionMatrix() const;
	void lookAt(DirectX::XMFLOAT3 at);

	void setAspect(const float& aspectX, const float& aspectY);
	float& fov();
	float& near_plane();
	float& far_plane();

private:
	float m_AspectX;
	float m_AspectY;
	float m_Fov;
	float m_Near;
	float m_Far;
};

