#pragma once

#include "stdafx.h"
#include "Painter2D.h"
#include <MyGAL/FortuneAlgorithm.h>
#include "TerrainGenerator.h"

class VoronoiTerrainGenerator {
public:
	VoronoiTerrainGenerator(int32_t nbVoronoiSites = 500, int32_t nbRelaxations = 2);
	VoronoiTerrainGenerator(std::tuple<int32_t, int32_t> params = std::tuple<int32_t, int32_t>(500, 2));

	Mesh generateTerrain(const DXDevice& device);

private:
	int32_t m_NbVoronoiSites;
	int32_t m_NbRelaxations;

	enum Biome {
		WATER = 1,
		OCEAN = 2,
		LAND = 4,
		COAST = 8,
		NB_BIOMES
	};

	struct TerrainChunk;

	struct TerrainCorner {
		mygal::Vector2<float> point{ 0.f, 0.f };	// 2D coordinate [-1.0f, 1.0f]
		std::vector<TerrainChunk*> surroundings{};	// Chunks around the corner
		std::vector<TerrainCorner*> neighbours{};	// Neighbours to the corner
		uint16_t biome = 0;		// Biome flags
		uint16_t index = 0;		// Index in the TerrainGraph array
		float height = -1.f;	// Height
	};

	struct TerrainChunk {
		mygal::Vector2<float> point{ 0.f, 0.f };	// 2D coordinate [-1.0f, 1.0f]
		std::vector<TerrainCorner*> corners{};		// Corners delimiting the chunk
		std::vector<TerrainChunk*> neighbours{};	// Neighbours to the chunk
		bool isBorder = false;	// Is the chunk on the border of the terrain
		uint16_t biome = 0;		// Biome flags
		uint16_t index = 0;		// Index in the TerrainGraph array
		float height = -1.f;	// Height
	};

	struct TerrainGraph {
		std::vector<TerrainChunk> chunks;		// Chunks of the Terrain
		std::vector<TerrainCorner> corners;		// Corners of the Terrain
	};

	std::vector<mygal::Vector2<float>> m_Points;
	std::unique_ptr<TerrainGraph> m_TerrainGraph;

	std::vector<mygal::Vector2<float>> generateRandomPoints(size_t nbPoints, float minX, float minY, float maxX, float maxY) const;
	mygal::Diagram<float> generateDiagram() const;
	Mesh generateDiagramMesh(const TerrainGraph& terrain) const;
	mygal::Diagram<float> generateRelaxedDiagram(size_t nbIterations);
	std::unique_ptr<TerrainGraph> generateIsland(const mygal::Diagram<float>& diagram) const;
	void setOceansAndCoasts(TerrainGraph& terrain, bool (*pred)(mygal::Vector2<float>, float));
	void setIslandHeight(TerrainGraph& terrain);

	static Painter2D::Color paramsToColor(uint16_t biome, float height);
	//Painter2D::Point2D<int16_t> pointToTexcoord(mygal::Vector2<float> point) const;
	//std::vector<Painter2D::Point2D<int16_t>> cornersToTexcoord(const std::vector<TerrainCorner*> corners) const;
	//void drawDiagramOnTexture(const DXDevice& device, const mygal::Diagram<float>& diagram);
	//void drawChunksOnTexture(const DXDevice& device, const std::vector<TerrainChunk>& chunks);

	static TerrainChunk* findChunk(std::vector<TerrainChunk>& chunks, const mygal::Vector2<float>& point);
	static TerrainCorner* findCorner(std::vector<TerrainCorner>& corners, const mygal::Vector2<float>& point);

	static bool IslandPred_Radial(mygal::Vector2<float> point, float threshold);
	static bool IslandPred_Perlin(mygal::Vector2<float> point, float threshold); // not implemented
};