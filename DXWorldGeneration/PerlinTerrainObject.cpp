#include "stdafx.h"
#include "PerlinTerrainObject.h"

PerlinTerrainObject::PerlinTerrainObject(std::string name, const DXDevice& device):
	SceneObject(name),
	m_TerrainData{ 1.0f, 1.0f },
	m_TerrainDataIndex(0)
{
	auto material = std::make_shared<Material>(device);
	material->ambient() = DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	m_TerrainDataIndex = material->createDedicatedBuffer(device, 2, sizeof(TerrainShaderData));
	material->updateDedicatedBuffer(device, m_TerrainDataIndex, &m_TerrainData);
	material->setPipeline(std::make_shared<DXPipeline>(device, "TerrainVS.cso", "VertexColorPS.cso"));

	Mesh plane = Mesh::createPlane(device, 100);
	plane.setMaterial(material);

	addMesh(plane);

	m_Scale = DirectX::XMFLOAT3(15, 15, 15);
}

PerlinTerrainObject::~PerlinTerrainObject()
{
}

void PerlinTerrainObject::render_imgui()
{
	ImGui::SliderFloat("Terrain Frequency", &m_TerrainData.frequency, 0.1f, 20.f, "%0.1f", 0);
	ImGui::SliderFloat("Terrain Exponent", &m_TerrainData.exponent, 0.1f, 20.f, "%0.1f", 0);
}

void PerlinTerrainObject::update(const DXDevice& device)
{
	m_Meshes[0].material()->updateDedicatedBuffer(device, m_TerrainDataIndex, &m_TerrainData);
	SceneNode::update(device);
}
