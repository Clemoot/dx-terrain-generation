#pragma once

#include "stdafx.h"
#include "DXRenderTexture.h"
#include "DXDepthStencilTexture.h"
#include "Window.h"

class DXSwapchain
{
public:
	DXSwapchain(const Window& window, const DXDevice& device);
	~DXSwapchain();

	void clear(const DXDevice& device, const float teal[4], const float depth, const uint8_t stencil);
	void present() const;
	void resize(const DXDevice& device);
	void setTarget(const DXDevice& device) const;

	const Microsoft::WRL::ComPtr<IDXGISwapChain>& swapchain() const;
	const float aspect() const;
	const DXRenderTexture& renderTarget() const;
	const DXDepthStencilTexture& depthStencil() const;


private:
	Microsoft::WRL::ComPtr<IDXGISwapChain> m_Swapchain;
	DXRenderTexture m_RenderTarget;
	DXDepthStencilTexture m_DepthStencil;
};

