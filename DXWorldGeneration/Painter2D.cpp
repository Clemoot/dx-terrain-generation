#include "stdafx.h"
#include "Painter2D.h"

Painter2D::Painter2D(uint8_t* const* const data, size_t width, size_t height) :
	m_Width(width),
	m_Height(height),
	m_Data(reinterpret_cast<Color* const* const>(data))
{
}

Painter2D::Painter2D(Color* const* const colors, size_t width, size_t height):
	m_Width(width),
	m_Height(height),
	m_Data(colors)
{
}

void Painter2D::drawLine(Point2D<int16_t> start, Point2D<int16_t> end)
{
	const unsigned nb_steps = 50U;
	float step_x = (end.x - start.x) / static_cast<float>(nb_steps);
	float step_y = (end.y - start.y) / static_cast<float>(nb_steps);

	float x = start.x, y = start.y;

	for (unsigned i = 0; i < nb_steps; i++) {
		int16_t ix = static_cast<int16_t>(x);
		int16_t iy = static_cast<int16_t>(y);
		m_Data[iy][ix] = { 0U, 255U, 0U, 255U };
		m_Data[iy - 1][ix] = { 0U, 255U, 0U, 255U };
		m_Data[iy][ix - 1] = { 0U, 255U, 0U, 255U };
		m_Data[iy + 1][ix] = { 0U, 255U, 0U, 255U };
		m_Data[iy][ix + 1] = { 0U, 255U, 0U, 255U };
		x += step_x;
		y += step_y;
	}
}

void Painter2D::drawPoint(Point2D<int16_t> point, Color color)
{
	const int size = 4;
	for (int i = -size; i <= size; i++) {
		for (int j = -size; j <= size; j++)
			m_Data[point.y + i][point.x + j] = color;
	}
}

void Painter2D::drawFace(const std::vector<Point2D<int16_t>>& points, Color color)
{
	for (size_t x = 0; x < m_Width; x++)
		for (size_t y = 0; y < m_Height; y++) {
			char prevSign = 0;

			// Check if pixel is in face
			bool pointInFace = true;
			uint16_t pos = 0;
			uint16_t neg = 0;
			for (size_t i = 0, nbPoints = points.size(); i < nbPoints; i++) {
				const Point2D<int16_t>& a = points[i];
				const Point2D<int16_t>& b = points[(i + 1) % nbPoints];

				Point2D<int> edge{ b.x - a.x, b.y - a.y };
				Point2D<int> point{ static_cast<int16_t>(x) - a.x, static_cast<int16_t>(y) - a.y };

				char cosine_sign = sign<int>(edge.x * point.y - edge.y * point.x);
				if (cosine_sign > 0) pos++;
				if (cosine_sign < 0) neg++;

				if (pos > 0 && neg > 0) {
					pointInFace = false;
					break;
				}
			}

			if (pointInFace)
				m_Data[y][x] = color;
		}
}