#include "stdafx.h"
#include "DXDepthStencilTexture.h"

DXDepthStencilTexture::DXDepthStencilTexture() : DXTexture2D(), m_View(nullptr)
{
}

DXDepthStencilTexture::DXDepthStencilTexture(const DXDevice& device, const uint16_t width, const uint16_t height) :
	DXTexture2D(device, CD3D11_TEXTURE2D_DESC(
		DXGI_FORMAT_D24_UNORM_S8_UINT,
		static_cast<uint32_t>(width),
		static_cast<uint32_t>(height),
		1,
		1,
		D3D11_BIND_DEPTH_STENCIL
	)),
	m_View(nullptr)
{
	CD3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc(D3D11_DSV_DIMENSION_TEXTURE2D);
	device.device()->CreateDepthStencilView(m_Texture.Get(), &depthStencilViewDesc, m_View.GetAddressOf());
}

DXDepthStencilTexture::~DXDepthStencilTexture()
{
	m_View.Reset();
}

const Microsoft::WRL::ComPtr<ID3D11DepthStencilView>& DXDepthStencilTexture::view() const
{
	return m_View;
}
