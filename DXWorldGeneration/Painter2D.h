#pragma once

#include "stdafx.h"

class Painter2D
{
public:
	template<typename T>
	struct Point2D {
		T x, y;
	};

	union Color {
		uint8_t comp[4];
		struct {
			uint8_t r;
			uint8_t g;
			uint8_t b;
			uint8_t a;
		};
	};

	Painter2D(uint8_t* const* const data, size_t width, size_t height);
	Painter2D(Color* const* const colors, size_t width, size_t height);

	void drawLine(Point2D<int16_t> start, Point2D<int16_t> end);
	void drawPoint(Point2D<int16_t> point, Color color);
	void drawFace(const std::vector<Point2D<int16_t>>& points, Color color);

private:
	Color* const* const m_Data;
	size_t m_Width, m_Height;

	template<typename T>
	inline char sign(T value) {
		T t0 = T(0);
		if (t0 == value)
			return 0;
		return (T(0) < value) ? 1 : -1;
	}
};

