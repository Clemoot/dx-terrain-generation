#pragma once

#include "GenerativeTerrain.h"


template<class T>
class TerrainContainer : public SceneObject
{
public:
	TerrainContainer(const std::string& terrainName, const DXDevice& device) :
		SceneObject("Terrain Container"),
		m_TerrainName(terrainName),
		m_Terrain(std::make_unique<T>(m_TerrainName, device)),
		m_GenerateNewTerrain(false)
	{
		m_Terrain->setParent(this);
	}

	template<typename... _Args>
	TerrainContainer(const std::string& terrainName, const DXDevice& device, _Args&&... params) :
		SceneObject("Terrain Container"),
		m_TerrainName(terrainName),
		m_Terrain(std::make_unique<T>(m_TerrainName, device, std::tuple<_Args...>(params...))),
		m_GenerateNewTerrain(false)
	{
		m_Terrain->setParent(this);
	}

	~TerrainContainer()
	{
		m_Terrain.release();
	}

	virtual void render_imgui() {
		if (ImGui::Button("Generate new terrain")) {
			m_GenerateNewTerrain = true;
		}
	}
	virtual void update(const DXDevice& device) {
		if (m_GenerateNewTerrain) {
			auto params = m_Terrain->genParams();
			m_Terrain = std::make_unique<T>(m_TerrainName, device, params);
			m_Terrain->setParent(this);
			m_GenerateNewTerrain = false;
		}
	}

	const virtual std::vector<Mesh>& meshes() const {
		return m_Terrain->meshes();
	}

private:
	std::string m_TerrainName;
	bool m_GenerateNewTerrain;

	std::unique_ptr<T> m_Terrain;
};