
# DX World Generation

This project is a study on terrain generation. It also a first step to understand how Direct3D works.

Two generation procedure are implemented:
**1. Perlin-noise terrain generation** : a common technique based on Perlin noise
**2. Voronoi island generation** : a technique based on Voronoi diagrams that essentially generates an island

Both algorithms can be tuned using widgets, powered by ImGui.

![Voronoi image](/images/Voronoi.jpg)

**Video:** [Youtube video](https://www.youtube.com/watch?v=rzOgd8G9ohc)
